
1. create a new user on MySQL db with permission on `tree_of_life` and `pfam`

create newuser 'phydo' @ '%' identified by '<your password>';

grant all privileges on pfam . * to 'phydo' @ '%';
grant all privileges on tree_of_life . * to 'phydo' @ '%';

2. modify /etc/mysql/mysql.conf.d/mysqld.cnf to change `bind-address' from '127.0.0.1' to '0.0.0.0' to allow remote access

3. restart mysql server by
`service mysql restart`

4. make sure your server's firewall opens up for outside connections

`sudo iptables -A INPUT -p tcp --dport 3306 -j ACCEPT`

^^^ note that this setting is flushed everytime the system shuts down, google 'save firewall settings' to see how to make permenant changes

4.1 You also need to make sure that there is no firewall at the network level that blocks external connections to server's 3306 port.

5. test connection on another computer by `mysql -u phydo -h <your host name here> -P 3306 -p`
it should prompt for password, you should be able to login

6. set up PhyDo backend, do **NOT** use `install.sh`, on Ubuntu use the following
```bash
# not tested yet, use as a reference
sudo apt-get install -y libmysqlclient-dev
sudo pip install -r requirements.txt
```

on CentOS:
```bash
# not tested yet, use as a reference
yum install -y libmysqlclient-dev, python-pip, MySQL-python
pip install -r requirements.txt
```

6.1 create `config.py`, use dummy host or username for now

```
host='<database hostname>'
port=3306
username='<username here>'
password='<password here>'
database='tree_of_life'
```

6.2 run `python app.py` to check if app can be started

7. set up apache WSGI, see reference http://flask.pocoo.org/docs/0.12/deploying/mod_wsgi/#creating-a-wsgi-file

7.1 `apt-get install libapache2-mod-wsgi` or `yum install mod_wsgi`

run `httpd -M | grep wsgi` to make sure WSGI is installed

7.2 The following is the APACHE config set-up that worked on CentOS

```
NameVirtualHost 129.97.83.59:80

<VirtualHost 129.97.83.59:80>
    ServerName biotools.uwaterloo.ca
    ServerAlias biotools
    ServerAdmin rt@rt.uwaterloo.ca
    DocumentRoot /var/www/html
    Options ExecCGI
    #WSGIDaemonProcess phydo-backend user=h329chen group=apache2 threads=5
    WSGIScriptAlias /phydo-api /var/www/phydo-backend/app.wsgi
    <Directory /var/www/phydo-backend>
        AllowOverride all
        # WSGIProcessGroup phydo-backend
         WSGIApplicationGroup %{GLOBAL}
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```


7.3 restart apache by running `systemctl restart httpd` on CentOS, ubuntu should be `service apache2 restart`

7.4 check `http://biotools.uwaterloo.ca/phydo-api/phydo/treeNodes/by/domains` it should say *Method not allowed* instead of any other error.

8. Database set up

Import SQL on doxtools (`/data/phydo/phydo-db-backup-2017-11-20.sql.tar.gz`) to an appropriate MySQL db server, note that `CREATE DATABASE` statements are already included in the SQL file

8.1 In the case of importing to biotool's MariaDB, an error is encountered in the following section, because we don't have `root`: 
```sql
CREATE DEFINER=`root`@`localhost` FUNCTION `get_common_ancestor`(t1 int, t2 int) RETURNS int(11)
BEGIN
DECLARE prev_id int;
```

We have to manually import all SQL functions, using SQL listed below:

```sql
USE tree_of_life;
DELIMITER //
DROP FUNCTION IF EXISTS tree_of_life.get_common_ancestor //
CREATE FUNCTION tree_of_life.get_common_ancestor(t1 int, t2 int)  RETURNS INT(11) DETERMINISTIC
BEGIN
DECLARE prev_id int;

IF NOT EXISTS (SELECT * FROM pfam.taxonomy WHERE ncbi_taxid = t1) OR NOT EXISTS (SELECT * FROM pfam.taxonomy WHERE ncbi_taxid = t2) THEN 
RETURN 0;
END IF;

DROP TEMPORARY TABLE IF EXISTS path_to_root;
CREATE TEMPORARY TABLE path_to_root(
    tax_id INT(11) PRIMARY KEY
);
SET prev_id = t1;
WHILE prev_id > 1 DO 
    INSERT INTO path_to_root (tax_id) VALUES(prev_id); 
    SELECT parent INTO prev_id FROM pfam.taxonomy WHERE ncbi_taxid = prev_id;
END WHILE;
INSERT INTO path_to_root (tax_id) VALUES (1); -- insert root

SET prev_id = t2; -- reinitialize to t2

WHILE NOT EXISTS (SELECT * FROM path_to_root WHERE tax_id = prev_id) DO
    SELECT parent INTO prev_id FROM pfam.taxonomy WHERE ncbi_taxid = prev_id;
END WHILE;

DROP TEMPORARY TABLE path_to_root;
RETURN prev_id;
END //
DELIMITER ;


DELIMITER //
DROP FUNCTION IF EXISTS pfam.get_genus //
CREATE FUNCTION pfam.get_genus(id int(10) UNSIGNED) RETURNS INT(10)  UNSIGNED DETERMINISTIC
BEGIN
DECLARE prev_id int;
DECLARE parent_id int;
DECLARE curr_rank VARCHAR(100);

SET prev_id = id;
SET parent_id = NULL;
SET curr_rank = 'no rank';
SELECT parent,rank INTO parent_id, curr_rank FROM pfam.taxonomy WHERE 
ncbi_taxid = prev_id;
WHILE parent_id > 0 AND parent_id != prev_id AND curr_rank != 'genus' DO
    SET prev_id = parent_id; 
    SET parent_id = NULL; 
    SELECT parent,rank INTO parent_id, curr_rank FROM pfam.taxonomy WHERE 
ncbi_taxid = prev_id;
END WHILE;
RETURN prev_id;
END //
DELIMITER ;
```

Also, we need to run a few manual processing SQL:

```sql
-- manually tag some internal nodes as a specific phyla

-- 3010 to Firmicutes (1239)
update pruned_node SET phylum_tax_id = 1239 where id = 3010;

-- 12 to Proteobacteria (1224)
update pruned_node SET phylum_tax_id = 1224 where id = 12;

update pfam.taxonomy set parent = 200795 WHERE level = 'Thermobaculum';
UPDATE tree_of_life.pruned_node SET phylum_tax_id = 200795 WHERE id = 2828;


-- view export in SQL seems to have a problem, manually fixing
CREATE OR REPLACE VIEW tree_of_life.pfam_tree_counts AS select COUNT(DISTINCT proteome_tax_id) as counts, genus_id from tree_of_life.pfam_tree GROUP BY genus_id ORDER BY counts DESC;
```

8.2 Change `config.py` in `phydo-backend` as the following:

```
host='<database hostname>'
port=3306
username='<username here>'
password='<password here>'
database='tree_of_life'
```

8.3 run `systemctl restart httpd` on centOS, then check http://biotools.uwaterloo.ca/phydo-api/phydo/unprunedTree to see if json can be returned, if yes, db connection is set up properly.

9. setting up `phydo-frontend`

set up `phydo-frontend` by cloning a repo

9.0 in `app/Config.js` change `export const SERVER_BASE_URL` to the following:
```
export const SERVER_BASE_URL = 'http://biotools.uwaterloo.ca/phydo-api';
```

9.1 mv `phydo-frontend` to `/var/www/phydo-frontend`, run `chown -R <your username>:<your username>  .` so that files do not belong to `root`

9.2 install npm

On CentOS as the following:
```bash
wget https://nodejs.org/download/release/latest/node-v9.11.1-linux-x64.tar.gz
sudo tar --strip-components 1 -xzvf node-v* -C /usr/local
# check installation
node --version
# outputs v9.11.1
```
9.3 `cd phydo-frontend` run `npm install` to install dependencies

9.4 `npm run build` to build everything in `public` folder

9.5 run `ln -s /var/www/phydo-frontend/public /var/www/html/phydo`

9.6 open up http://biotools.uwaterloo.ca/phydo, web page should open, and if backend is set up properly as well, then tree should show.