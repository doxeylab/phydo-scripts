Written by Han Chen on May 27, 2017

Procedures: 

1. modify database tables as instructed below
2. go to `scripts/link-tree-pfam`, run `main.sh <mysqlrootpassword>`
3. go to `scripts/export_tree_from_db` run `python ./export_tree_db.py --root_password $password` replacing `$password` with mysql root password

To Verify:

* tree export script should not fail
* If you add one more protein domain data, say `PF00700` flagellin C domain, and assign the ncbi taxonomy ID under human. Open up web interface and search flagellin C domain, `homo sapien` should light up.

Database Tables to modify: 

Input: Assuming you have the following from hmmscan

seq_id, pfamA_acc [the pfam domain acc], nacbi_taxid [taxonomy id of the organism]

1. under `pfamseq` table, add `ncbi_taxid`, seq_id as `pfam_seqid`, and create a unique `auto_architecture` for that sequence, which does not conflict with any `auto_architecture` in existing `architecture` table
2. under `architecture` table, use the same `auto_architecture` as above, and `architecture_acc` which lists pfam accession number in order like `PF07679 PF13895 PF07679 PF13927`
3. under `pfam_A_reg_full_significant` table, add an entry for all domains in a sequence, with the following columns, `pfamseq_acc`, `ali_start` [this can be from your hmmscan result], `pfamA_acc`, `pfamseq_acc`

Any column not listed above is not actively used by the project's scripts. Use any dummy value should be fine.



