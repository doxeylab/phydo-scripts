Running and Installing
----
We use NPM to manage dependencies, so run `npm install` under project root to install all dependencies.
run `npm run start` to start server in localhost:8080
run `npm run build` to build everything in the `public` folder

Overview
-----
The main technologies used are _React_, _D3_ and _mobx_. Only the tree and the pie chart are drawn using D3 and they are contained in a react component, mobx can help manage application state (similar to Angular's $scope) and patch UI upon any update. The rest of documentation is broken up into features.


A little bit about event cycle
-----
Any user action captured in React component is dispatched using a singleton class `AppDispatcher`, which will then dispatch the action to all registered action handlers, e.g. `QueryBoxActionHandler`. Each action object has a `type` for action handler to decide whether to respond. Typically the store (application state) object is modified by action handler to update UI. 

Please consult mobx's documentation on why mutating store's property automatically triggers UI re-rendering.

Querying
---- 
We exemplify logic and structure through cases.

E.g. User typed domain name and clicked "GO"

1. an action with type `QUERY_SUBMITTED` is dispatched from `QueryBoxContainer.js`
2. `AppDispatcher` dispatches the action to all registered handlers, as initialized in `App.js`
3. `QueryBoxActionHandler.js` decides to respond to the event by calling `handleQuerySubmitted`
4. `QueryBoxStore` contains the current application state and is used to retrieve relevant info. The result is processed then submitted to the server via a call to `queryService.queryDomains` 
5. `treeStore` and `summaryStore` are modified to reflect changes. 

Tree
------
* `TreeContainer.js` contains buttons and and the main tree
* `TreeOfLife.js` is a React component wrapper for D3 tree drawing
* Tree update logic: Whenever `TreeStore` is modified, the `autorun()` lambda function inside `TreeOfLife` is run to trigger D3's re-rendering. 

Example: user clicks on an internal node in tree

1. since click events on D3 drawn tree are handled by D3, we need to pre-register event handler callbacks to `TreeDrawer.js`. This is achieved by calling `treeDrawer.bindEvent` in `TreeOfLife`. 
2. After user's click, callbacks in `TreeOfLife` are called, and actions are dispatched using `AppDispatcher`
3. `TreeActionHandler` handles the action, and a `nodeDetail` object is manufactured inside `TreeStore` by calling `setNodeDetail`
4. This mutates `TreeStore` and UI inside `TreeContainer` will be automatically updated. In particular, since `treeStore.nodeDetail` is not null, `NodeDetailContainer` will be rendered out, resulting a pop up box.

TreeStore
------
In tree store, we maintain several application states:

- `masterTree`: a tree object; although appearing as a method, this is used as a property, see mobx `@computed` documentation; open debugger and see how this object is like.
- `displayRoot`: also a tree object, but unlike `masterTree`, it may not contain the entire tree, nodes that are below certain depth level are removed. And, this is a DEEP COPY of `masterTree`. 
- `displayRoot` vs `masterTree`: In rendering UI, we some times wish to modify how a tree is displayed (e.g. modify branch length, remove certain nodes) but we should also keep a master copy of tree of life. Hence `displayRoot` concerns only the partial tree shown on screen, and `masterTree` is a master copy and should never be changed.
- `highlightedNodes` vs `highlightedTaxNodes`: the former is a set of all nodes highlighted via **domain search**, the latter being from **taxonomy search**
- `currentDisplayLevel` indicates the current tree viewing resolution. e.g. phylum, genus








