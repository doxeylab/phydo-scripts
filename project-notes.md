Existing Tools
===================
TreeDom: http://treedom.sbc.su.se/index.php (does not work for my uniprot gene)

TreeFam: http://www.treefam.org/ (can't do microbial gene)

PhylomeDb: http://phylomedb.org/ (does not work for my uniprot gene)

Fun Tree: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4702901/

TreeDomViewer: http://nar.oxfordjournals.org/content/34/suppl_2/W104.abstract

ITol, Tree Graph Maker: http://itol.embl.de/tree/12997124208322501476303124

Curated Tree: https://tree.opentreeoflife.org


Visualization
=============

BioJS
Cytoscape js
D3 js
talk to TreeDom people

D3 is the way to go



Data Manipulation
=========
but this depends on the types of query asked
python igraph offers good graph API
consider map them to Newick.Tree, then to graph form 
we must allow efficient graph manipulation

Labeling Tree
--------
APE in R tree manipulation, reading ,parse



Data Collection
===============

JGI
----
- At the start of 2016, IMG/M had a total of 38,395 genome datasets from all domains of life and 8,077 microbiome datasets, out of which 33,116 genome datasets and 4,615 microbiome datasets are publicly available. Note that some genome and microbiome datasets have been deleted because of sequencing or annotation quality problems or have been replaced by newer versions.
- uses PFAM V29, GO, July 2015, TIGRfam(release 15)
- Downloading: see instruction, http://genome.jgi.doe.gov/help/download.jsf download with globus
    + https://www.globus.org/app/transfer?origin_id=78312867-6d04-11e5-ba46-22000b92c6ec&origin_path=%2F~%2FBy_Data_Source_And_Type%2FAnnotation%2F
    + 

IMG Bacteria -> All -> IMG/M -> IMG (integrated ) ->




JAMN: JAMN data -> NCBI Assembly -> WGS -> download FASTA
SAMN: biosample -> assembly

IMG: select genome matching the names -> add to genome cart -> click on genome name -> click on with Pfam -> get a pfam table

PICrust paper might already build a table to associate functional traits with 16s at species level (references might lead to useful data)


Each species, CP is candidate phyla
CPR super phyla
Microgenomates OP_11 
otherwise it has family, order,species built in

Exception phylas
candidate phyla, microgenomates, parcubacteria 

take a loook at img.jgi, browse genomes 
need to ask JGI for help 
30 species in staphylococcus

Download NCBI Proteome, with a Taxa Id
------------------
http://seqanswers.com/forums/showthread.php?t=29193

pfam files: ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/



Challenge: 
genome species is large, 4500 genomes 



Selling Points
==============
dcGO needs a tree of life??? 



- discovery tools to visualize for LGT (complements my domain analysis)
- 







