#!/usr/bin/python

# make sure to have beautiful soup installed

from bs4 import BeautifulSoup
import requests
import unittest
import re
import csv
import argparse 
import os,sys




def parse_to_soup(content):
    return BeautifulSoup(content,'html5lib')

def parse_soup_from_file(fileName):
    content = ''
    with open(fileName,'r') as f:
        content = f.read()
    return parse_to_soup(content)

_species_link_re = 'https://img\.jgi\.doe\.gov/cgi-bin/m/main\.cgi\?section=TaxonDetail&amp;taxon_oid='
def is_species_tag(tag):
    if tag.name != 'a' or not tag.has_attr('href'):
        return False
    matched = re.search(_species_link_re,tag['href'])
    return matched != None

def get_taxoid(tag):
    result = re.search(_species_link_re + '(\d+)',tag['href'])
    return result.group(0)

def get_row(tag):
    link = 'https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=TaxonDetail&amp;taxon_oid='
    taxoid = get_taxoid(tag)
    name = tag.text.strip()
    return [name,taxoid,link+taxoid]

def get_rows_from_soup(jgiHtml):
    """
    jgiHtml is a beautiful soup object
    """
    all_species_links = jgiHtml.find_all(is_species_tag)
    rows = [get_row(link) for link in all_species_links]
    return rows

def write_to_csv(path,rows):
    assert(len(rows) == 0 or len(rows[0]) == 3)
    with open(path,'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['name','taxoid','link'])
        for r in rows: 
            writer.writerow(r)


def make_dir_if_missing(dir):
    if not dir:
        print >> sys.stderr, 'Empty directory given. Exiting.'
        sys.exit(1)
    elif not os.path.isdir(dir):
        try:
            os.mkdir(dir)
        except:
            print >> sys.stderr, 'Cannot create directory \'%s\'. Exiting.' % dir 
            sys.exit(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""
        Given a jgi taxonomy html page, scrapes the link containing relevant species info. 
        input: html
        output: tab delimited file
        e.g. 
        name\ttaxoid\tlink
        Aigarchaeota archaeon JGI 0000001-A7 (contamination screened) (GBS-A_001_112)\t2264867219\thttps://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=TaxonDetail&amp;taxon_oid=2264867219
        """)
    parser.add_argument('--jgi_html', dest='jgi_html',help='html file from jgi', required=True)
    parser.add_argument('--output_path', dest='output_path',help='output file',required=True)
    args = parser.parse_args()
    soup = parse_soup_from_file(args.jgi_html)
    rows =get_rows_from_soup(soup)

    write_to_csv(args.output_path,rows)





