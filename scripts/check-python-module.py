import imp
import sys

if len(sys.argv) <= 1:
    print "no arg found"
    exit(1)

try:
    imp.find_module(sys.argv[1])
except ImportError:
    exit(1)

