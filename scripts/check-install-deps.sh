install(){
    deps=$@
    sudo apt-get install -y $deps
}


if [ ! `which wget` ]; then
    install wget
fi 

if [ ! `which python` ]; then 
    install python 
fi 

if [ ! `which pip` ]; then 
    install python-setuptools python-dev build-essential
    sudo easy_install pip
fi

if [ ! `which curl` ]; then
    install curl
fi

install_python_mysql(){
    sudo apt-get install -y libmysqlclient-dev
    sudo pip install MySQL-python==1.2.5
}

remove_apt_get_python_mysqldb(){
    sudo apt-get remove -y python-mysqldb
}

python ./check-python-module.py MySQLdb

if [ $? -ne 0 ]; then 
    install_python_mysql
else
    python -c "
import MySQLdb
if (MySQLdb.__version__ < '1.2.5'):
     exit(1)
"
    if [ $? -ne 0 ]; then
        remove_apt_get_python_mysqldb
        install_python_mysql
    fi
fi

python ./check-python-module.py Bio

if [ $? -ne 0 ]; then
    sudo pip install biopython
fi

if [ ! `which mysql` ]; then
    echo "need mysql, it is not installed, please install it"
    echo "check this website https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-14-04"
    echo "The following instruction can be used:"
    echo "sudo apt-get install -y mysql-server"
    echo "sudo mysql_secure_installation"
    echo "sudo mysql_install_db"
    exit 1
fi

echo "Completed checking"

