

Assuming you have already the SQL database set up

1. Run all export_data.sql in your mysql console
2. Run install-neo4j.sh to install db and start the database
3. Put the exported csv file into neo4j/import folder
4. add appropriate header to each file (some don't have header)
5. run the cypher load commands in neo4j shell



Starting instance: 

make sure you uncomment dbms.connector.http.address=0.0.0.0:7474
if wanting to access server remotely
```bash
./bin/neo4j start -config conf/neo4j.conf
```

References: 

https://neo4j.com/developer/guide-import-csv/
https://neo4j.com/docs/operations-manual/current/tutorial/import-tool/