USING PERIODIC COMMIT 500 load csv with headers from 'file:///node_with_header.csv' AS line CREATE(n:TreeNode{id:toInt(line.id),name:line.name,length:toFloat(line.length),isLeaf:(CASE line.isLeaf WHEN '1' THEN TRUE ELSE FALSE END), depth:toInt(line.depth),comment:line.comment});

CREATE INDEX ON :TreeNode(id);

USING PERIODIC COMMIT 500 LOAD CSV WITH HEADERS FROM 'file:///taxnode_with_header.csv' AS line CREATE(TaxNode{ncbi_taxid:line.id,level:line.level,rank:line.rank,species:(CASE line.species WHEN 'NULL' THEN '' ELSE line.species END)});

CREATE INDEX ON :TaxNode(ncbi_taxid);

USING PERIODIC COMMIT 500 LOAD CSV FROM 'file:///tree_node_relationship.csv' AS line MATCH (child:TreeNode{id:toInt(line[0])}), (parent:TreeNode{id:toInt(line[1])})  WHERE line[0] != '1' CREATE (parent) - [pof:ParentOf] -> (child);

USING PERIODIC COMMIT 500 LOAD CSV FROM 'file:///tree_tax_node_relationship.csv' AS line MATCH (treeNode:TreeNode{id:toInt(line[0])}), (taxNode:TaxNode{ncbi_taxid:toInt(line[1])}) WHERE line[1] != '', MATCH (genusNode:Taxnode{ncbi_taxid:toInt(line[2])}) WHERE line[2] != '' CREATE (treeNode) - [:assignedTo] -> (taxNode),(treeNode) - [:representingGenus] -> (genusNode);

USING PERIODIC COMMIT 500 LOAD CSV FROM 'file:///tree_node_relationship.csv' AS line MATCH (child:TaxNode{ncbi_taxid:toInt(line[0])}), (parent:TaxNode{ncbi_taxid:toInt(line[1])})  WHERE line[0] != '1' CREATE (parent) - [pof:ParentOf] -> (child);

USING PERIODIC COMMIT 500 LOAD CSV FROM 'file:///proteomes.csv' AS line CREATE (proteome:Proteome{ncbi_taxid:toInt(line[0]),numProteins:toInt(line[1]),totalGenomeProteins:toInt(line[2]),totalSeqsCovered:toInt(line[3])});

USING PERIODIC COMMIT 500 LOAD CSV FROM 'file:///proteome_node_relationship.csv' AS line MATCH(p:Proteome{ncbi_taxid:toInt(line[0])}), (t:TreeNode{id:toInt(line[1])}) CREATE (p) - [:AnnotatingNode] -> (t);



