SELECT id, IFNULL(name,'') AS name, length, 
IFNULL(is_leaf,'0') AS is_leaf, depth, 
IFNULL(comment,'') as comment FROM tree_of_life.node 
INTO outfile 
'/var/lib/mysql-files/node_concise_null_as_empty.csv'
 FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
 ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT IFNULL(ncbi_taxid,'') AS ncbi_taxid, IFNULL(level,'') AS level, 
IFNULL(rank,'') AS rank, IFNULL(species,'') AS species FROM pfam.taxonomy
INTO outfile '/var/lib/mysql-files/taxonomy_concise_null_as_empty.csv'
 FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
 ESCAPED BY '"' LINES TERMINATED BY '\n';


SELECT id, IFNULL(parent_id,'') AS parent_id FROM tree_of_life.node 
INTO outfile '/var/lib/mysql-files/tree_node_relationship.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT n.id, IFNULL(n.tax_id,'') AS tax_id, IFNULL(ng.genus_id,'') AS genus_id
FROM tree_of_life.node n LEFT JOIN tree_of_life.node_genus ng ON ng.node_id = n.id
INTO outfile '/var/lib/mysql-files/tree_tax_node_relationship.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT ncbi_taxid, parent FROM pfam.taxonomy
INTO outfile '/var/lib/mysql-files/tax_node_relationship.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
ESCAPED BY '"' LINES TERMINATED BY '\n';

SELECT ncbi_taxid, num_proteins, total_genome_proteins, 
total_seqs_covered FROM pfam.complete_proteomes
INTO outfile '/var/lib/mysql-files/proteomes.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
ESCAPED BY '"' LINES TERMINATED BY '\n';

-- note that multiple proteomes can map to the same node
SELECT proteome_tax_id, tree_node_id FROM tree_of_life.pfam_tree 
INTO outfile '/var/lib/mysql-files/proteome_node_relationship.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ',' 
ESCAPED BY '"' LINES TERMINATED BY '\n';




