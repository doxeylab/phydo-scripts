if [! `which neo4j` ]; then
    wget -O - https://debian.neo4j.org/neotechnology.gpg.key | sudo apt-key add -
    echo 'deb http://debian.neo4j.org/repo stable/' | sudo tee /etc/apt/sources.list.d/neo4j.list
    sudo apt-get update
    sudo apt-get install -y neo4j
    sudo echo "NEO4J_ULIMIT_NOFILE=60000" >> /etc/default/neo4j
    if [ ! `which neo4j` ]; then
        echo 'Something is wrong with installation'
        echo 'I cant find neo4j'
        exit 1
    fi
fi

sudo sed -i 's/^#dbms.connector.http.address=0.0.0.0:7474/dbms.connector.http.address=0.0.0.0:7474/' /etc/neo4j/neo4j.conf




