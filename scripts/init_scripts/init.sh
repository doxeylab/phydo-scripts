#!/bin/bash

# suitable as a start up init.d script

# start phydofrontend and backend scripts in /home/han directory

phydobackend="/home/han/phydo-backend"
phydofrontend="/home/han/phydo-frontend"

frontend_pid=${phydofrontend}/public/frontend.pid
backend_pid=${phydobackend}/backend.pid

do_start(){
	sudo iptables -A INPUT -p tcp --dport 8000 -j ACCEPT
	sudo iptables -A INPUT -p tcp --dport 5000 -j ACCEPT
	cur=`pwd`
	cd ${phydofrontend} && npm run build && cd public/;
	python -m SimpleHTTPServer &> frontend.log & echo $! > frontend.pid
	cd $cur
	cd ${phydobackend}
	./flask/bin/python app.py &> backend.log &
	wait $!
	ps -ef | grep "${phydobackend}/flask/bin/python" | grep -v grep | awk '{print $2}' > backend.pid
	cd $cur
}

check_file(){
	if [ ! -d  ${phydobackend} ]; then
		>&2 echo "expected phydobackend path ${phydobackend} not found"
		exit 1
	fi
	if [ ! -d  ${phydofrontend} ]; then
		>&2 echo "expected frontend path ${phydofrontend} not found"
		exit 2
	fi
}

do_stop(){
	if [ -r $frontend_pid ];then
		kill "$(cat $frontend_pid)"
		rm $frontend_pid
	fi
	if [ -r $backend_pid ];then
		kill "$(cat $backend_pid)"
		rm $backend_pid
	fi
}

do_status(){
	if [ ! -r $frontend_pid ];then
		>&2 echo "frontend pid not found"
		exit 1
	fi
	if [ ! -r $backend_pid ];then
		>&2 echo "backend pid not found"
		exit 1
	fi
	fpid=$(cat $frontend_pid)
	bpid=$(cat $backend_pid)
	echo "frontend running at $fpid , backend running at $bpid"
}

check_file

case "$1" in
	start|"")
		do_start
		do_status
		;;
	stop)
		do_stop
		;;
	status)
		do_status
		;;
	restart)
		do_stop
		do_start
	;;
	*)
	>&2 echo "Usage $0 (start|stop|restart|status)"
	exit 1
	;;
esac

exit 0



