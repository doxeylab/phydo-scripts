-- creates a MYSQL table called pfam_tree that joins tree node to ncbi taxonomy id

USE tree_of_life;

DROP TABLE IF EXISTS pfam_tree;
CREATE TABLE pfam_tree(
    proteome_tax_id INT(11),
    tree_node_id INT(11),
    genus_id INT(11)
);

CREATE OR REPLACE VIEW node_tax AS select t.rank, t.level, t.ncbi_taxid,r.id from pfam.taxonomy t inner join tree_of_life.node r ON t.ncbi_taxid = r.tax_id;
CREATE OR REPLACE VIEW pfam.proteome_tax AS select t.* from pfam.complete_proteomes c inner join pfam.taxonomy t ON t.ncbi_taxid=c.ncbi_taxid;
CREATE OR REPLACE VIEW pfam.proteome_tax_count AS select COUNT(*) as counts, t.rank from pfam.complete_proteomes c inner join pfam.taxonomy t ON t.ncbi_taxid=c.ncbi_taxid group by t.rank;

DELIMITER //
DROP FUNCTION IF EXISTS pfam.get_genus //
CREATE FUNCTION pfam.get_genus(id int(10) UNSIGNED) RETURNS INT(10) UNSIGNED DETERMINISTIC
BEGIN
DECLARE prev_id int;
DECLARE parent_id int;
DECLARE curr_rank VARCHAR(100);

SET prev_id = id;
SET parent_id = NULL;
SET curr_rank = 'no rank';
SELECT parent,rank INTO parent_id, curr_rank FROM pfam.taxonomy WHERE 
ncbi_taxid = prev_id;
WHILE parent_id > 0 AND parent_id != prev_id AND curr_rank != 'genus' DO
    SET prev_id = parent_id; 
    SET parent_id = NULL; 
    SELECT parent,rank INTO parent_id, curr_rank FROM pfam.taxonomy WHERE 
ncbi_taxid = prev_id;
END WHILE;
RETURN prev_id;
END //
DELIMITER ;

USE pfam;
DROP TABLE IF EXISTS proteome_genus;
CREATE TABLE proteome_genus(
    ncbi_taxid INT(11) PRIMARY KEY,
    genus_id INT(11)
);

-- link proteomes into its corresponding genus_id, assuming all proteomes are mapped to genus_level or below
INSERT INTO pfam.proteome_genus (ncbi_taxid,genus_id) SELECT ncbi_taxid,pfam.get_genus(c.ncbi_taxid) AS genus_id FROM pfam.complete_proteomes c;

USE tree_of_life;

DROP TABLE IF EXISTS node_genus;
CREATE TABLE node_genus(
    node_id INT(11) PRIMARY KEY, 
    ncbi_taxid INT(11),
    genus_id INT(11)
);

-- link each node with tax_id to its genus_id, assuming all leaves are at genus level or below (and it is true)
INSERT INTO node_genus (node_id,ncbi_taxid,genus_id) SELECT n.id, n.tax_id, pfam.get_genus(n.tax_id) AS genus_id FROM tree_of_life.node n WHERE n.is_leaf = 1;

DROP TABLE IF EXISTS node_genus_counts;
CREATE TABLE node_genus_counts(
    genus_id INT(11) PRIMARY KEY,
    counts INT(8)
);

-- create a table where `counts` is the number of tree leaves under that genus_id
INSERT INTO  tree_of_life.node_genus_counts (genus_id,counts) SELECT  genus_id,COUNT(*) AS counts from node_genus GROUP BY genus_id HAVING genus_id IS NOT NULL;
-- Example queries, 
-- SELECT COUNT(*) AS counts, genus_id, rank,id from node_genus GROUP BY genus_id LIMIT 20;
-- Counts how many unique genus there are in the tree of life 

-- select COUNT(*) from node_genus ng join pfam.taxonomy t ON ng.ncbi_taxid = t.ncbi_taxid WHERE ng.genus_id = 1 AND t.taxonomy LIKE '%metagenome%';
-- counts the number of genomes that are classified as metagenomes


-- we are assuming that each node leaf corresponds to one genus, however, we still have multiple nodes 
-- mapping onto the same genus, in the table `node_genus_counts`. 
-- We don't want to deal with that case, so we only link proteomes to tree nodes with unique genus_id 
INSERT INTO tree_of_life.pfam_tree 
(proteome_tax_id,tree_node_id,genus_id) 
SELECT p.ncbi_taxid,ng.node_id,ng.genus_id FROM pfam.proteome_genus p
INNER JOIN tree_of_life.node_genus ng ON p.genus_id = ng.genus_id 
INNER JOIN tree_of_life.node_genus_counts ngc ON ng.genus_id = ngc.genus_id
WHERE 
ng.genus_id != 1 AND 
ngc.counts = 1;

CREATE OR REPLACE VIEW tree_of_life.pfam_tree_counts AS select COUNT(DISTINCT proteome_tax_id) as counts, genus_id from tree_of_life.pfam_tree GROUP BY genus_id ORDER BY counts DESC;

