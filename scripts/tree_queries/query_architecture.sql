
use tree_of_life;
DELIMITER //
DROP PROCEDURE IF EXISTS tree_of_life.select_nodes_at_depth;
CREATE PROCEDURE tree_of_life.select_nodes_at_depth(IN depth INT(4) , IN architecture_pattern VARCHAR(512))
BEGIN
 SET group_concat_max_len = 32768; -- 32kb should be enough for ids
 -- select all nodes in tree less than or equal to specified depth, along with all seq_ids 
 -- that grouped to that node
 SELECT c.seq_ids, n.id, n.length, n.tax_id, n.name FROM 
    -- join all sequences by proteome_tax_ids with tree nodes, then group them
    -- by node parent ids at a specific depth
   (SELECT GROUP_CONCAT(a.seq_ids) as seq_ids, get_parent_at_depth(tp.tree_node_id,depth) AS node_id FROM (
                 -- select all sequences that match the architecture pattern
                 SELECT GROUP_CONCAT(seq.pfamseq_id) as seq_ids, \
                               seq.ncbi_taxid \
                        FROM   pfam.architecture arch, pfam.pfamseq seq 
                        WHERE  arch.architecture_acc LIKE architecture_pattern \
                        AND arch.auto_architecture = seq.auto_architecture
                        GROUP BY seq.ncbi_taxid
                 ) AS a, tree_of_life.pfam_tree as tp WHERE a.ncbi_taxid = tp.proteome_tax_id 
             GROUP BY get_parent_at_depth(tp.tree_node_id,depth))
   AS c 
   RIGHT JOIN tree_of_life.node n ON c.node_id = n.id
   WHERE n.depth <= depth;
END//
DELIMITER ;

