-- given a node id in `node`
-- finds parent of this node at specified depth
-- if node is already lower than specified depth, just return node id

use tree_of_life;

DELIMITER //
DROP FUNCTION IF EXISTS tree_of_life.get_parent_at_depth;
CREATE FUNCTION tree_of_life.get_parent_at_depth(n INT(11), d INT(4)) RETURNS INT(11)
BEGIN 
DECLARE curr_id INT(11);
DECLARE curr_depth INT(4);
IF NOT EXISTS (SELECT * FROM tree_of_life.node WHERE id = n) THEN 
RETURN 0;
END IF;
SET curr_id = n;
SELECT depth INTO curr_depth FROM tree_of_life.node WHERE id = n; 
WHILE curr_depth > d DO
SELECT depth, parent_id INTO curr_depth, curr_id FROM tree_of_life.node WHERE id = curr_id;
END WHILE;

RETURN curr_id;

END //

DELIMITER ;
