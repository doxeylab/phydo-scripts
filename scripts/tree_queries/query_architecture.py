import MySQLdb
from MySQLdb.constants import FIELD_TYPE
import sys, argparse
import json
def getDb(username,password,host='localhost',database='tree_of_life'):
    _my_conv = { FIELD_TYPE.LONG: int,FIELD_TYPE.INT24:int}
    conn = MySQLdb.connect(user=username,passwd=password,host=host,db=database,conv=_my_conv)
    return conn

def getSimplifiedRows(db):
    db.query("""CALL select_nodes_at_depth(11,'PF00669%PF00700');""")
    result = db.store_result()
    rows = result.fetch_row(maxrows=0,how=1) # fetch in dict form
    return rows

def getSimplifiedNodeMap(db):
    rows = getSimplifiedRows(db)
    return [{'id': r['id'], 'seqIds':r['seq_ids'].split(',')} for r in rows if r['seq_ids']]

def getCompleteRows(db):
    db.query("""CALL select_nodes_at_depth(999,'PF00669%PF00700');""")
    result = db.store_result()
    rows = result.fetch_row(maxrows=0,how=1) # fetch in dict form
    return rows

def getCompleteNodeMap(db):
    rows = getCompleteRows(db)
    return [{'id': r['id'], 'seqIds':r['seq_ids'].split(',')} for r in rows if r['seq_ids']]

def toJson(m,file):
    json.dump(m,file)


if __name__ == '__main__':
    db = getDb('root','password')
    # with open('./complete_rows.json','w') as f:
        # toJson(getCompleteNodeMap(db),f)
    with open('./simplified_rows.json','w') as f:
        toJson(getSimplifiedNodeMap(db),f)






