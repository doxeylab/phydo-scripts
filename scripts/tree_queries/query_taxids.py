
"""
This file is a quick test, it is not used in production. 
It might be missing components as modifications happen
"""



import MySQLdb
from MySQLdb.constants import FIELD_TYPE
import sys, argparse
import json
def getDb(username,password,host='localhost',database='tree_of_life'):
    _my_conv = { FIELD_TYPE.LONG: int,FIELD_TYPE.INT24:int}
    conn = MySQLdb.connect(user=username,passwd=password,host=host,db=database,conv=_my_conv)
    return conn

def _getNodeId(db,taxid):
    sql = """SELECT node_id from tree_of_life.node_genus where pfam.get_genus(%s) = genus_id;"""
    db.query(sql);
    results = db.store_result()
    rows = results.fetchrow(rows=0,how=1)
    if len(rows) == 0:
        return None
    return rows[0]['node_id']

def getNodeIdFromTaxIds(db,taxids):
    node_ids = [_getNodeId(db,t) for t in taxids if ].filter(lambda x: x) # filter out NoneIds 
    return node_ids 




