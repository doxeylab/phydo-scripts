
if [ $# -ne 1 ]; then
    echo "Usage: $0 <root_password>"
    echo "loads sql procedures to corresponding database"
    exit 1
fi

root_password=$1

cat ./get_parent.sql ./query_architecture.sql | mysql -u root -p${root_password} || exit 1


