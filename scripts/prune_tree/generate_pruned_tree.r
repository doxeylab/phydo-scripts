# use install.packages("ape")

library(ape)
tree <- read.tree("tree.newick")
tips <- tree$tip.label
tipsToRemove <- read.csv("missgintTips.csv",header=F)
indicesToRemove <- match(tipsToRemove[,1],tips)
tree.new <- drop.tip(tree,indicesToRemove)
write.tree(tree.new, "pruned_tree.newick")


