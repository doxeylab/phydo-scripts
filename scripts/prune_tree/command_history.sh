python make_newick_tree_labelled_by_id.py --json ../../data/tree.json --newick ../../supplement-tree-of-life-Dataset_2_ToL_rp_concat_RAxML.tree --output ../../data/tree-labelled-by-id.newick

python missing_node_to_csv.py --tree_json ../../data/tree.json --output_csv ../../data/missing_proteome_nodes.csv


scp /media/han/Acer/Users/Han/waterloo/doxey_lab/tree-of-life-domain-arch/data/pruned_tree.newick han@doxtools.uwaterloo.ca:~/


echo "create table tree_of_life.pruned_node like node;" | mysql -u root -p -D tree_of_life

python load_pruned_tree.py --db_user root --db_name tree_of_life --table_name pruned_node --db_password han --tree ~/pruned_tree.newick


echo "UPDATE pruned_node pn JOIN node n ON pn.id = n.id 
SET pn.extern_id = n.extern_id, pn.name = n.name,
 pn.is_leaf = n.is_leaf, pn.comment = n.comment, 
 pn.tax_id = n.tax_id WHERE n.is_leaf = 1;" | mysql -u root -p -D tree_of_life


python compute_detail_level.py --root_password han --table_name tree_of_life.pruned_node

python export_tree_db.py --root_password han --table tree_of_life.pruned_node

scp han@doxtools.uwaterloo.ca:/home/han/_sc/export_tree_from_db/tree.json  /media/han/Acer/Users/Han/waterloo/doxey_lab/tree-of-life-domain-arch/data/pruned_tree.json



