import argparse
import json
import csv
parser = argparse.ArgumentParser(description="""
Accepts a JSON file representing the tree, 
outputs a csv file for all _leaves_ with `count`=0.
The csv contains only one column, which is the `id` attribute
""")
parser.add_argument('--tree_json', dest='tree_json', required=True, help='A json file representing the tree')
parser.add_argument('--output_csv', dest='output_csv', required=True, help='Where the csv output should go')


def load_json(jsonfile):
    jsonStr=''
    with open(jsonfile,'r') as f:
        jsonStr = f.read()
    print(len(jsonStr))
    return json.loads(jsonStr)




def find_leaves_to_remove(tree):
    if 'children' not in tree or tree['children'] is None or len(tree['children']) == 0:
        if tree['counts'] is None or int(tree['counts']) == 0:
            print "adding %s to the list to remove" % (tree['id'])
            return [tree['id']]
        return []
    return [x for c in tree['children'] for x in find_leaves_to_remove(c) ] #flatmap


def write_to_csv(names, output_name):
    with open(output_name, 'w') as f:
        writer = csv.writer(f,delimiter=',',quoting=csv.QUOTE_MINIMAL,quotechar='"')
        for n in names:
            writer.writerow([n])

if __name__ == '__main__':
    args = parser.parse_args()
    treeObj = load_json(args.tree_json)
    leaves_to_remove = find_leaves_to_remove(treeObj)
    write_to_csv(leaves_to_remove,args.output_csv)
