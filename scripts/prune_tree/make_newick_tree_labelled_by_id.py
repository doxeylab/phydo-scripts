import argparse
from Bio import Phylo
from newick_io_km import Writer # only change made to this file
import json

parser = argparse.ArgumentParser(description="""
Given a json file, and a newick file representing the same tree,
replace the label used in newick tree with the id from the json file
ON newnick_node.name = json_node.name
generates a newick
""")
parser.add_argument('--json',dest='json', required=True, help='json tree')
parser.add_argument('--newick',dest='newick', required=True, help='tree in newick format')
parser.add_argument('--output', dest='output', required=True, help='output location')



def get_newick_tree(treeFile):
    return Phylo.read(treeFile,'newick')

def get_json_tree(treeFile):
    with open(treeFile,'r') as f:
        jsonStr = f.read()
        return json.loads(jsonStr)

def _relabel_node(newick_node, json_node):
    # we need that newick_node.clades[0] corresponding to json_node['children'][0]
    newick_node.name = str(json_node['id'])
    for i in range(0,len(newick_node.clades)):
        _relabel_node(newick_node.clades[i],json_node['children'][i])
    return newick_node
def relabel_newick_tree(tree, jsonTree):
    _relabel_node(tree.clade, jsonTree)

def write_newick_file(newick_tree,output):
    Phylo.write(newick_tree,output,'newick')

if __name__ == '__main__':
    args = parser.parse_args()
    newick_tree = get_newick_tree(args.newick)
    json_tree = get_json_tree(args.json)
    relabel_newick_tree(newick_tree, json_tree)
    write_newick_file(newick_tree, args.output)



