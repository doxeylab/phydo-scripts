import argparse
import Bio

parser = argparse.ArugmentParser(description="""
Given a json file representing the unpruned tree,
and a newick file representing the pruned tree,
attach all metadata information from the unpruned to the pruned
    """)
parser.add_argument('--unpruned_json',dest='unpruned_json', required=True, help='unpruned tree')
parser.add_argument('--pruned_newick',dest='pruned_newick', required=True, help='pruned tree in newick format')
parser.add_argument('--output', dest='output', required=True, help='output location')


"""
given the name of newick file
parse it, return a json-like map with the following fields

{
    children,
    name,
    length,
    comment,
}

"""
def newick_to_tree(newickFile):
    pass 

def tree_to_node_map(newickFile):
    


if __name__ == '__main__':
    args = parser.parse_args()
    tree = newick_to_tree(args.newick)
    

