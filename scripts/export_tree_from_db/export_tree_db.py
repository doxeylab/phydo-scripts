import MySQLdb
from MySQLdb.constants import FIELD_TYPE
import os
import json
import unittest
import argparse
import sys 

"""

NOTE:

This script has been duplicated in tree_of_life_server repository. It might not be the most 
up to date version.
"""

def getDb(username,password,host='localhost',database='tree_of_life'):
    _my_conv = { FIELD_TYPE.LONG: int,FIELD_TYPE.INT24:int}
    conn = MySQLdb.connect(user=username,passwd=password,host=host,db=database,conv=_my_conv)
    return conn

def get_rows(db, table = None):
    sql = """SELECT n.id, n.parent_id, n.comment,n.length, n.name, n.tax_id, t.level,
    IF(n.is_leaf AND t.taxonomy IS NOT NULL, 
        IF(LOCATE('metagenome',t.taxonomy) OR LOCATE('environmental sample',t.taxonomy), TRUE,FALSE),
        NULL) AS is_uninformative_tax,
    t.rank,
    p.counts, ng.genus_id,n.is_leaf,n.detail_level from """+table+""" n 
    LEFT JOIN tree_of_life.node_genus ng ON n.id = ng.node_id
    LEFT JOIN tree_of_life.pfam_tree_counts p ON ng.genus_id = p.genus_id
    LEFT JOIN pfam.taxonomy t ON n.tax_id = t.ncbi_taxid
    ;"""
    db.query(sql)
    result = db.store_result()
    rows = result.fetch_row(maxrows=0,how=1)
    return rows


class TreeNode:
    def __init__(self,length=None,parentId=None,id=None,taxId=None,comment=None,
        name=None,counts=None,genusId=None,children = None,detailLevel=None,
        isLeaf=None,level=None,isUninformativeTax=None, rank=None):
        self.length = length 
        self.parentId = parentId
        self.id = id
        self.taxId = taxId
        self.comment = comment
        self.name = name
        self.counts = counts
        self.genusId = genusId
        self.children = children if children else list()
        self.detailLevel = detailLevel
        self.isLeaf = isLeaf
        self.level = level # this is the level name corresponding to labeled tax id
        self.rank = rank 
        self.isUninformativeTax = isUninformativeTax 
    def to_dict(self):
        return {
            'length': self.length ,
            'parentId': self.parentId,
            'id': self.id,
            'taxId': self.taxId,
            'comment': self.comment,
            'name': self.name,
            'counts': self.counts,
            'genusId': self.genusId,
            'children': [c.to_dict() for c in self.children],
            'detailLevel': self.detailLevel,
            'isLeaf': self.isLeaf,
            'level': self.level,
            'rank': self.rank,
            'isUninformativeTax': self.isUninformativeTax
        }
    def count_nodes(self):
        return 1+sum([c.count_nodes() for c in self.children])


def get_tree_json(root):
    return json.dumps(root.to_dict())

def row_to_node(row):
    return TreeNode(
        length=row['length'],
        parentId=row['parent_id'],
        id=row['id'],
        taxId=row['tax_id'],
        comment=row['comment'],
        name=row['name'],
        counts=row['counts'],
        genusId=row['genus_id'],
        detailLevel = row['detail_level'],
        isLeaf = None if row['is_leaf'] is None else int(row['is_leaf']) == 1,
        level = row['level'],
        rank = row['rank'],
        isUninformativeTax = None if row['is_uninformative_tax'] is None else int(row['is_uninformative_tax']) ==1
        )

def _link_tree(nodes,n): 
    # multiple fail safe checks
    if (not n.id) or (not n.parentId) or (n.id == n.parentId) or (n in nodes[n.parentId].children):
        return 
    parent = nodes[n.parentId]
    parent.children.append(n)
    _link_tree(nodes,parent)


def link_tree (nodes): 
    tree_nodes = nodes.values()
    for n in tree_nodes:
        _link_tree(nodes,n)

def _get_tree(rows):
    nodes = {r['id']:row_to_node(r) for r in rows }
    link_tree(nodes)
    return (nodes[1],nodes) # we know that the root node has id 1

def load_tree_db(username,password,table=None):
    db = getDb(username,password)
    rows = get_rows(db,table=table)
    root,nodes = _get_tree(rows)
    return (root,nodes)


class TestTree(unittest.TestCase):
    def setUp(self):
        self.db = getDb('root','password')
    def testLoadTree(self):
        db = self.db
        rows = get_rows(db)
        (root,node_map) = _get_tree(rows)
        self.assertEquals(len(node_map),6164)
        self.assertEquals(root.count_nodes(),6164)
    def testSerializeTree(self):
        tree = get_tree_json(TreeNode(length=1,id=1,comment='1234',name='root'))
        root = TreeNode(name='root',children=[TreeNode(name='child')])
        tree2 = get_tree_json(root)
        print(tree)
        print(tree2)
        self.assertEquals(len(tree),len('{"comment": "1234", "taxId": null, "counts": null, "children": [], "isLeaf": null, "name": "root", "level": null, "id": 1, "length": 1, "parentId": null, "detailLevel": null, "genusId": null}'))
        self.assertEquals(tree2,'{"comment": null, "taxId": null, "counts": null, "children": [{"comment": null, "taxId": null, "counts": null, "children": [], "isLeaf": null, "name": "child", "level": null, "id": null, "length": null, "parentId": null, "detailLevel": null, "genusId": null}], "isLeaf": null, "name": "root", "level": null, "id": null, "length": null, "parentId": null, "detailLevel": null, "genusId": null}')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='exports tree of life from database to `pwd`/tree.json file. Also runs ')
    parser.add_argument('--root_password',dest='root_password',required=True,help='MySQL root password')
    parser.add_argument('--table',dest='table',default='tree_of_life.node', required=False,help='The table from which tree is exported, default tree_of_life.node')
    args = parser.parse_args()
    filePath = os.path.join(os.getcwd(),'tree.json')
    with open(filePath,'w') as f:
        root,_ = load_tree_db('root',args.root_password,table=args.table)
        json.dump(root.to_dict(),f)
    # unittest.main()

