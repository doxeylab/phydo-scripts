#!/bin/bash
curDir=`pwd`

numArgs=2

if [ $# -ne $numArgs ]; then
    echo "Usage: $0 <TREE_FILE> <TREE_GENOME_TAXID_CSV>"
    echo "Please only use absolute path!"
    exit 1
fi

bash ./check-install-deps.sh || exit 1

TREE_FILE=$1
TREE_GENOME_TAXID_TSV=$2

echo -n "Please enter your password for mysql root:"
read -s password
if [ ! $password ]; then 
    echo "password should not be empty"
    echo "if you have an empty password, just set a new one"
    exit 2
fi
echo "This will be saved for scripts later. 
Note that you should have complete control over machines, because passwords can 
appear in command-line arguments"

echo "The following scripts (downloading part) can take very long. Be sure to see 'completed' as the final output, otherwise something is wrong"

echo "Begin downloading and loading databases"

cd ./load-pfam-db
bash ./import-pfam.sh $password || exit 4
cd $curDir

echo "updating pfam taxonomy"
cd ./update_pfam_taxonomy
bash ./update_db.sh $password || exit 5
cd $curDir

cd ./load-tree-db
bash ./main.sh $TREE_FILE $password $TREE_GENOME_TAXID_TSV || exit 3
cd $curDir

echo "Now we loaded all tables, performing additional processing"

cd ./link-tree-pfam
bash ./main.sh $password || exit 6
cd $curDir

cd ./compute_detail_level_and_depth
bash ./main.sh $password || exit 7
cd $curDir

cd ./tree_queries
bash ./load_procedures.sh $password || exit 8
cd $curDir

echo "Now we should have everything set up. We test by exporting tree of life in json format"

cd ./export_tree_from_db
python ./export_tree_db.py --root_password $password || exit 9
cd $curDir
echo "Completed importing unpruned tree. You should see a tree.json file here"






