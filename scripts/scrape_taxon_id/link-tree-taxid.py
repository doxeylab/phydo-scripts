import sys,subprocess,os
import argparse
from cStringIO import StringIO
import tempfile
import re
import csv
parser = argparse.ArgumentParser(description=
    """ given a list of files containing name 
        -> id entries scrapes the taxon id info 
        to each one
    """)
parser.add_argument('--id_entries',nargs='+',dest='id_entries',help='files containing id entries')
parser.add_argument('--output_dir',dest='output_dir',help='directory of output')
TMP_DIR='/tmp'

def check_file_exists(filepaths,filename=None, message=None):
  if type(filepaths) == list:
    if (len(filepaths) == 0):
      print >> "required files missing, %s" % filename
      sys.exit(1)
    for filepath in filepaths:
      if not os.path.isfile(filepath):
        if (not message): 
          print >> sys.stderr, '%s is not a file.' % filepath
        else:
          print >> sys.stderr, message
        sys.exit(1)
  elif type(filepaths) == str:
    filepath = filepaths
    if not os.path.isfile(filepath):
      if (not message): 
        print >> sys.stderr, '%s is not a file.' % filepath
      else:
        print >> sys.stderr, message
      sys.exit(1)
  else:
    raise "filepaths is either list or string"


def make_dir_if_missing(output):
    if not output:
        print >> sys.stderr, 'No output_dir given. Exiting.'
        sys.exit(1)
    elif not os.path.isdir(output):
        try:
            os.mkdir(output)
        except:
            print >> sys.stderr, 'Cannot create output dir \'%s\'. Exiting.' % output
            sys.exit(1)

# returns the file path of a file made in `dir`
def make_file(fileName='',dir=None):
    return os.path.join(dir,fileName)

def run_process(args, stdout_file=None):
    stdout_handle = None
    stdout_handle = (open(stdout_file, 'w') if stdout_file else
                     tempfile.NamedTemporaryFile(dir=TMP_DIR))
    stderr_handle = tempfile.NamedTemporaryFile(dir=TMP_DIR)
    with stdout_handle as stdout, stderr_handle as stderr:
        the_process = subprocess.Popen(args, stdout=stdout, stderr=stderr)
        print >> sys.stderr, 'Running commands: %s' % ' '.join(args)
        return_code = the_process.wait()
        output = ''
        with open(stdout_handle.name,'r') as f:
            output = f.read()
        return (return_code,output)

def isBiosample(id):
    return re.search('^SAMN', id)

def isNucId(id):
    isNC = bool(re.search('^N[A-Z]_', id))
    isGenBank = not isBiosample(id) and re.search('^[A-Z]{4}',id);
    return isNC or isGenBank


def isBioproject(id):
    return bool(re.search('^[A-Z]{5}\d{4}', id))

def isImgSubmission(id):
    return bool(re.search('^\d{4}\d+$', id))

def isJGI(id):
    return bool(re.search('^\d+$', id))

def get_taxon_id(id):
    return_code = 2
    result = ''
    if(isBiosample(id)):
        (return_code,result) = run_process(['casperjs','./casper_scrape_samn.js',str(id)])
    elif(isBioproject(id)):
        # go to https://www.ncbi.nlm.nih.gov/bioproject/ + id 
        # scrape for taxonomy
        (return_code,result) = run_process(['casperjs','./casper_scrape_bioproject.js',str(id)])
    elif(isNucId(id)):
        # go to https://www.ncbi.nlm.nih.gov/nuccore/ + id
        # scrape for taxonomy
        (return_code,result) = run_process(['casperjs','./casper_scrape_nuc.js',str(id)])
    elif(isJGI(id)):
        raise Exception('JGI not ready for scrape yet')

    if return_code != 0:
        return None
    else:
        return result

def get_taxon_ids(id_entry_file):
    success = []
    failed = []
    with open(id_entry_file,'r') as f:
        reader = csv.reader(f,delimiter='\t',quotechar='"')
        for row in reader:
            if len(row)==0:
                continue
            name = row[0]
            if len(row) < 3:
                print >> sys.stderr,"%s does not have enough columns" % name
                failed.append([name])
                continue
            id = row[2]
            if not id: 
                failed.append([name])
                print >> sys.stderr,"%s failed because id could not be found" % name
                continue
            else:
                taxon_id = get_taxon_id(id)
                if not taxon_id:
                    failed.append([name,id])
                    print >> sys.stderr, '%s failed because taxon id could not be scraped' % name
                else:
                    success.append([name,id,taxon_id.strip()])
    return (success,failed)


def write_to_csv(path,rows,header=None):
    with open(path,'wb') as csvfile: # must be binary
        writer = csv.writer(csvfile, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)
        if header:
            writer.writerow(header)
        for r in rows: 
            writer.writerow(r)

def run_program(id_entry,output_dir):
    (success_rows,failed_rows) = get_taxon_ids(id_entry)
    id_entry_name = id_entry.split('/')[-1]
    success_path = make_file(fileName='succ_'+id_entry_name+'.tsv',dir=output_dir)
    failed_path = make_file(fileName='fail_'+id_entry_name+'.tsv',dir=output_dir)
    write_to_csv(success_path,success_rows)
    write_to_csv(failed_path,failed_rows)



if __name__ == '__main__':
    args = parser.parse_args()
    check_file_exists(args.id_entries)
    make_dir_if_missing(args.output_dir)
    for id_entry in args.id_entries:
        run_program(id_entry,args.output_dir)
