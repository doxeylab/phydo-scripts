#!/bin/bash

if [ ! `which node` ]; then
    echo "Node is required to run npm"
    if [ ! `which nodejs` ]; then 
        echo "installing node"
        sudo apt-get install -y nodejs 
    fi
    sudo ln -s `which nodejs` "/usr/bin/node"
fi

if [ ! `which npm` ]; then
    echo "installing npm"
    sudo apt-get install -y npm
fi

sudo npm install -g casperjs
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
tar -xvjf phantomjs*.tar.bz2
rm phantomjs*.tar.bz2
# making it executable across the globe
sudo mv phantomjs*/bin/phantomjs /usr/local/bin/

echo "the following should be the casper version"
casperjs --version

