if [ $# -lt 2 ]; then
    echo "usage: $0 <accession id file> <output dir> <additional_flags>"
    echo "the script parallelizes link-tree-taxid.py, additional_flags will be passed to the python scraper"
    exit 1
fi

PREFIX='PARALLELIZED_ACC_'

accFile=$1
outputDir=$2
shift
shift
additionalFlags=$@
echo "splitting"
split -l 5 $accFile $PREFIX
echo "running job at"
date
PIDFILE="${outputDir}/processes.pid"
rm ${PIDFILE}
for file in ${PREFIX}* ;do
    python ./generalized_scraper.py --id_entries ${file} --output_dir ${outputDir} ${additionalFlags} & echo $! >> ${PIDFILE}
done

wait `cat ${PIDFILE}`
if [ $? -ne 0 ];then 
    echo "job failed"
    rm ${PREFIX}*
    rm ${PIDFILE}
    exit 1
fi
rm ${PIDFILE}
echo "reducing result at"
date
baseFileName=$(basename "$accFile")
# note that successful outputs are prefixed with succ by our python scripts
cat ${outputDir}/"succ_"*${PREFIX}* > ${outputDir}/"succ_processed_${baseFileName}.tsv"
cat ${outputDir}/"fail_"*${PREFIX}* > ${outputDir}/"fail_processed_${baseFileName}.tsv"


