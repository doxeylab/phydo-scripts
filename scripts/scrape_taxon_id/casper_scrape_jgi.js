var casper = require('casper').create({
    // pageSettings: {
    //     loadImages: false,
    //     loadPlugins: true,
    //     userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36'
    //   }
});

casper.options.pageSettings.javascriptEnabled = true;
casper.start().thenOpen('https://img.jgi.doe.gov/cgi-bin/m/main.cgi',{
    method:'post',
    headers: {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36',
        'content-type':'application/x-www-form-urlencoded',
        'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    },
    data:{
        'taxonTerm':'21516',
        'page':'orgsearch',
        'section':'TaxonSearch',
        '_section_TaxonSearch_x':'Go'
    }
});

// casper.then(function(){
//     var FORM = 'form[name="taxonSearchForm"]';
//     this.waitForSelector(FORM,function(){
//         this.echo("hhelo, reached waiting stage");
//         this.fill(FORM,{
            
//         },true);
//     },function fail(){
//         this.echo("form not found");
//         this.exit(1);
//     });
// });

casper.then(function(){
    casper.options.waitTimeout = 10000;
    this.waitForSelector('div#taxontable_section',function(){
        this.click('td.yui-dt0-col-GenomeNameSampleName a');
    },function fail(){
        this.echo(this.getHTML());
        this.echo('cannot find section');
        this.exit(1);
    });
});

casper.then(function(){
    var ncbiRow = '#content_other > form > table > tbody > tr:nth-child(5)';
    this.waitForSelector(ncbiRow,function(){
        var rowHeader = this.fetchText(ncbiRow + ' > th');
        if(!/NCBI Taxon ID/.test(rowHeader)){
            this.exit(1);
        }
        var taxonId = this.fetchText(ncbiRow + ' > td > a');
        this.echo(taxonId);
        this.exit(0);
    });
});

casper.then(function(){
    this.echo('function finished, unknown error');
    this.exit(1);
});

casper.run();

