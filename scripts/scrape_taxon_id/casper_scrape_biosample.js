var casper = require('casper').create();

var id = casper.cli.args[0];
casper.start('https://www.ncbi.nlm.nih.gov/biosample/'+id,function(){
    this.waitForSelector('.docsum',function(){
        var html = this.getHTML('.docsum');
        var regex = /\/taxonomy\/(\d+)/;
        var id = html.match(regex)[1];
        this.echo(id);
        this.exit(0);
    });
});

casper.then(function(){
    this.exit(1);
});

casper.run();