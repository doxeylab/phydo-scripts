var casper = require('casper').create();

var id = casper.cli.args[0];
casper.start('https://www.ncbi.nlm.nih.gov/bioproject/'+id+'/',function(){
    this.waitForSelector('span.TID',function(){
        var taxText = this.fetchText('span.TID');
        var regex = /Taxonomy ID: (\d+)/;
        this.echo(taxText.match(regex)[1]);
        this.exit(0);
    });
});

casper.then(function(){
    this.exit(1);
});

casper.run();