var casper = require('casper').create();

var id = casper.cli.args[0];
casper.start('https://www.ncbi.nlm.nih.gov/nuccore/'+id);

function searchTaxonId(html){
    var regex = /<a href="\/Taxonomy\/Browser\/wwwtax\.cgi\?id=(\d+)/;
    var taxonId = html.match(regex);
    return taxonId;
}
casper.waitForSelector('pre.genbank');

casper.waitFor(function(){
        var result = searchTaxonId(this.getHTML('pre.genbank'));
        return result.length > 1;
    },function then(){
        var id = searchTaxonId(this.getHTML('pre.genbank'))[1];
        this.echo(id);
        this.exit(0);
    },function timeout(){

    });

casper.then(function(){
    this.exit(1);
});

casper.run();

