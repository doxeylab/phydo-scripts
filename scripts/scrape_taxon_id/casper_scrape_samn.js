var casper = require('casper').create();

var SAMNID = casper.cli.args[0];
casper.start('https://www.ncbi.nlm.nih.gov/assembly/?term=' + SAMNID,function(){
    var selector = '#EntrezForm div.supplemental.col.three_col.last';
    return this.waitForSelector(selector,function(){
        var html = this.getHTML(selector);
        var regex = /href="(\/taxonomy\?.*\d+)"/;
        var link = html.match(regex)[1];
        link = link.replace(/&amp;/g,'&');
            return casper.open('https://www.ncbi.nlm.nih.gov'+link);
    });
});

casper.then(function(){
    var html = this.getHTML('#maincontent > div');
    var regex = /\/Taxonomy\/Browser\/wwwtax\.cgi\?id=(\d+)/;
    this.echo(html.match(regex)[1]);
    this.exit(0);
});

casper.then(function(){
    this.exit(1);
});

casper.run();

