#!/bin/bash

if [  $# -ne 1 ]; then
    echo "Usage: $0 <password>"
    echo "need mysql root password"
    exit 1
fi

password=$1

mysql -u root -p${password} -D tree_of_life < ./create_util_functions.sql

python ./compute_detail_level.py --root_password $password || exit 1


