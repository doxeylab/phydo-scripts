
# for each leaf node
# find all siblings remove its siblings from queue to iterate
# compute the common ancestor for all siblings, put it on parent
# find parent's siblings, recursively compute parent tax id if not present already
# do the same thing for parent's parent etc etc 
import sys
import argparse
if __name__ == '__main__' and __package__ is None:
    import sys, os
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),'..')))
    import export_tree_from_db.export_tree_db as tExpt


# top down
# for any node, 
# if it is a leaf, just return the tax id, if tax id is none, then return 0
# if all of children tax ids are known, 
# filter out any children with zero tax id
# if only one child left, just use that id
# else, compute common ancestor for all children
# else if some children's tax id are not known, recursively compute tax id for that

def is_int_or_long(n):
    return type(n) == int or type(n) == long

def get_common_ancestor(t1,t2,db):
    assert is_int_or_long(t1)
    assert is_int_or_long(t2)
    sql = 'SELECT tree_of_life.get_common_ancestor(%s, %s);' % (t1,t2)
    db.query(sql)
    result = db.store_result()
    rows = result.fetch_row(maxrows=0)
    ancestor = rows[0][0]
    return ancestor # 0 if invalid t1, t2 

def _compute_tax_id(node,db):
    if len(node.children) == 0:
        if node.taxId is None or node.isUninformativeTax:
            node.taxId = 0
        return
    children = node.children 
    for c in children: 
        _compute_tax_id(c,db)
    taxIds = [c.taxId for c in children if c.taxId != 0]
    if len(taxIds) == 0:
        node.taxId = 0
    else:
        common_ancestor_id = reduce(lambda acc,x: get_common_ancestor(acc,x,db),taxIds)
        node.taxId = common_ancestor_id
    print >> sys.stderr, 'computed common_ancestor for node %s, with ancestor id %s' % (node.id, node.taxId)

def save_tax_id_detail_level(nodes,db, table=None):
    c = db.cursor()
    # convert nodes to rows
    rows = [(n.taxId,n.detailLevel, n.phylum_tax_id if hasattr(n, 'phylum_tax_id') else -1, 
            n.class_tax_id if hasattr(n, 'class_tax_id') else -1, n.id) 
            for n in nodes]
    assert (len(rows) > 0)
    c.executemany("""UPDATE """ + table+ """ SET tax_id=%s,detail_level=%s, phylum_tax_id = %s, class_tax_id = %s WHERE id = %s; """,rows)

def _compute_detail_level(node, _level=1):
    node.detailLevel = _level
    for c in node.children:
        newLevel = _level+1 if c.taxId != node.taxId else _level
        _compute_detail_level(c, _level=newLevel)

def clean_tax_id(db, table=None):
    cursor = db.cursor();
    cursor.execute("""UPDATE """+table+ """ SET tax_id = NULL WHERE is_leaf is NULL OR is_leaf = 0;""");
    cursor.execute("""UPDATE """+table+ """ SET detail_level = NULL;""")
    cursor.execute("""UPDATE """+table+ """ SET phylum_tax_id = NULL, class_tax_id = NULL;""")
    cursor.close()
    db.commit()

# accepts a dictionary cursor
def db_get_parent(cursor,tax_id):
    if tax_id == 0 or tax_id == 1:
        return 0
    cursor.execute("""SELECT parent FROM pfam.taxonomy WHERE ncbi_taxid = %s""",[tax_id])
    rows = cursor.fetchall()
    if (len(rows) == 0):
        return 0
    return int(rows[0][0])

def db_get_rank(cursor,tax_id):
    if tax_id == 0 or tax_id == 1:
        return None
    cursor.execute("""SELECT rank FROM pfam.taxonomy WHERE ncbi_taxid = %s""", [tax_id])
    rows = cursor.fetchall()
    if (len(rows) == 0):
        return None
    return rows[0][0]

def get_rank_taxid(db,rank,node_taxid):
    cursor = db.cursor()
    taxid = node_taxid
    while taxid != 0:
        r = db_get_rank(cursor,taxid)
        if rank == r:
            return taxid
        taxid = db_get_parent(cursor,taxid)
    return 0
"""
Assign tax id for either phylum or class 
e.g. assign phylum_tax_id to be Chordata's tax id for all nodes under Chordata
 or assign class_tax_id to be the belonging <class>'s tax id for all levels under <class>
 if no such class or phylum exist for leaf nodes, e.g. some nodes do not have inferred phylum or class ancestor
 then give `phylum_tax_id` 0 and `class_tax_id` 0
"""
def assign_rank(node,db,rank):
    r = get_rank_taxid(db,rank,node.taxId)
    print ('assigning rank to %d' % node.id)
    if r != 0 or len(node.children) == 0:
        print ('found rank tax id %d for node %d' % (r,node.id))
        if rank == 'phylum':
            node.phylum_tax_id = r
        elif rank == 'class':
            node.class_tax_id = r
        else:
            raise 'Unsupported rank %s' % rank
        return
    for c in node.children:
        assign_rank(c,db,rank)

def _compute_phylum_and_class_rank(db, root):
    assign_rank(root,db,'phylum')
    assign_rank(root,db,'class')


if __name__ == '__main__' and __package__ is None:
    parser = argparse.ArgumentParser(description='exports tree of life from database to `pwd`/tree.json file. Also runs ')
    parser.add_argument('--root_password',dest='root_password',required=True,help='MySQL root password')
    parser.add_argument('--table_name',dest='table_name',default='tree_of_life.node', required=False,help='database prefixed table length, default, tree_of_life.node')
    args = parser.parse_args()
    db = tExpt.getDb('root', args.root_password)
    clean_tax_id(db,table=args.table_name)
    root, nodes = tExpt.load_tree_db('root', args.root_password, table=args.table_name)
    _compute_tax_id(root, db)
    _compute_detail_level(root)
    _compute_phylum_and_class_rank(db,root)
    save_tax_id_detail_level(nodes.values(), db, table=args.table_name)
    db.commit()


