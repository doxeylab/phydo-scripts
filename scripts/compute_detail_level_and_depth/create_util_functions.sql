
-- given two taxa id, computes the common ancestor for those 2
-- precondition: t1 and t2 are valid tax ids in taxonomy table
-- this needs to be loaded into tree_of_life database first
USE tree_of_life;
DELIMITER //
DROP FUNCTION IF EXISTS tree_of_life.get_common_ancestor //
CREATE FUNCTION tree_of_life.get_common_ancestor(t1 int, t2 int)  RETURNS INT(11) DETERMINISTIC
BEGIN
DECLARE prev_id int;

IF NOT EXISTS (SELECT * FROM pfam.taxonomy WHERE ncbi_taxid = t1) OR NOT EXISTS (SELECT * FROM pfam.taxonomy WHERE ncbi_taxid = t2) THEN 
RETURN 0;
END IF;

DROP TEMPORARY TABLE IF EXISTS path_to_root;
CREATE TEMPORARY TABLE path_to_root(
    tax_id INT(11) PRIMARY KEY
);
SET prev_id = t1;
WHILE prev_id > 1 DO 
    INSERT INTO path_to_root (tax_id) VALUES(prev_id); 
    SELECT parent INTO prev_id FROM pfam.taxonomy WHERE ncbi_taxid = prev_id;
END WHILE;
INSERT INTO path_to_root (tax_id) VALUES (1); -- insert root

SET prev_id = t2; -- reinitialize to t2

WHILE NOT EXISTS (SELECT * FROM path_to_root WHERE tax_id = prev_id) DO
    SELECT parent INTO prev_id FROM pfam.taxonomy WHERE ncbi_taxid = prev_id;
END WHILE;

DROP TEMPORARY TABLE path_to_root;
RETURN prev_id;
END //
DELIMITER ;


DELIMITER //
DROP PROCEDURE IF EXISTS tree_of_life.update_depth //
CREATE PROCEDURE tree_of_life.update_depth()
BEGIN 
-- first make sure the column depth exists
IF (NOT EXISTS(SELECT *  FROM information_schema.COLUMNS  WHERE      TABLE_SCHEMA = 'tree_of_life'  AND TABLE_NAME = 'node'  AND COLUMN_NAME = 'depth'))
THEN ALTER TABLE tree_of_life.node ADD COLUMN depth INT(4);
END IF;

-- root depth = 1
UPDATE tree_of_life.node SET depth = 0 WHERE id = 1;
-- compute depth here
WHILE (EXISTS (SELECT * FROM tree_of_life.node WHERE depth is NULL)) DO
    UPDATE tree_of_life.node n1 INNER JOIN tree_of_life.node n2 ON n1.parent_id = n2.id SET n1.depth = (n2.depth + 1) 
    WHERE n2.depth IS NOT NULL AND n1.id != 1;
END WHILE;
END //
DELIMITER ;
call update_depth();
