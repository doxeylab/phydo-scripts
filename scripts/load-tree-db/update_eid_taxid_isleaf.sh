
if [ $# -ne 1 ]; then
    echo "usage: $0 <tree node to taxid csv file>"
    echo "the csv file must have absolute path"
    echo "the csv file should be generated from scraping"
    exit 1
fi 

MY_CSV=$1

# a bit hard to read, but it works
# the temp table idea is taken from here: 
# http://stackoverflow.com/questions/15271202/mysql-load-data-infile-with-on-duplicate-key-update
echo "CREATE TEMPORARY TABLE temp_node LIKE node;
LOAD DATA LOCAL INFILE '${MY_CSV}' INTO TABLE temp_node 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' 
LINES TERMINATED BY '\n' (name, extern_id, tax_id);
UPDATE node n JOIN temp_node tn ON n.name = tn.name 
SET n.extern_id=tn.extern_id, n.tax_id = tn.tax_id;
UPDATE node n1 RIGHT OUTER JOIN node n2 ON 
n1.parent_id = n2.id set n2.is_leaf=1 where n1.id is NULL;
" | mysql -u root -p -D tree_of_life



