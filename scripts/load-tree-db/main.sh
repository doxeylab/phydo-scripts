NUM_ARGS=3

if [ $# -ne $NUM_ARGS ];then
    echo "Creates a database called tree_of_life, loads all data into a table called node"
    echo "usage: $0 <TREE_FILE> <ROOT_PASSWORD> <TREE_GENOME_TAXID_CSV>"
    echo "please provide absolute path for TREE_GENOME_TAXID_CSV"
    echo "We need a comma separated file where the first column is name, second is external id, third is tax id"
    echo "you will be prompted mysql root password again after scripts run"
    exit 1
fi 

TREE_FILE=$1
ROOT_PASSWORD=$2
TREE_GENOME_TAXID_CSV=$3
mysql -u root -p < "create_tree.sql" \
&& \
python ./load_tree_db.py --tree $TREE_FILE --db_password $ROOT_PASSWORD \
&& \
bash update_eid_taxid_isleaf.sh $TREE_GENOME_TAXID_CSV || exit 1








