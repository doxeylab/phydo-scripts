from Bio import Phylo
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
import sys, argparse
def getDb(username,password,host='localhost',database='tree_of_life'):
    _my_conv = { FIELD_TYPE.LONG: int,FIELD_TYPE.INT24:int}
    conn = MySQLdb.connect(user=username,passwd=password,host=host,db=database,conv=_my_conv)
    return conn

def parseNewick(fileName):
    tree = Phylo.read(fileName,'newick')
    return tree


parser = argparse.ArgumentParser(description='Load newick tree to db')
parser.add_argument('--db_user', dest='db_user', default='root', help='Mysql DB username, default root', required=False)
parser.add_argument('--db_name', dest='db_name', default='tree_of_life', help='db name, default tree_of_life', required=False)
parser.add_argument('--table_name', dest='table_name', default='node', help='db table name, default node', required=False)
parser.add_argument('--db_password', dest='db_password',help='Mysql DB password',required=True)
parser.add_argument('--tree', dest='tree',help='tree filepath',required=True)

def insert_row(db,name=None,length=None,comment=None,parent_id=None,table=None):
    name = ("'" + name + "'" ) if name else 'NULL'
    length = str(length) if length else 'NULL'
    comment = ("'" + comment + "'" ) if comment else 'NULL'
    parent_id = parent_id or 'NULL'
    query = """INSERT INTO %s (name,length,comment,parent_id)
                    VALUES(%s,%s,%s,%s)""" % (table,name,length,comment,parent_id)
    cursor = db.cursor()
    cursor.execute(query)
    rowid = cursor.lastrowid
    return rowid


def _load_tree_db(db,clade,parent_id=None,table=None):
    name = clade.name
    length = clade.branch_length
    comment = clade.comment
    parent = insert_row(db,name=name,length=length,comment=comment,parent_id=parent_id,table=table)
    for child in clade.clades:
        _load_tree_db(db,child,parent_id=parent,table=table)

def load_tree_db(db,tree,table='node'):
    _load_tree_db(db,tree.root,table=table)
    db.commit()

if __name__ == '__main__':
    args = parser.parse_args()
    db = getDb(args.db_user,args.db_password, database=args.db_name)
    tree = parseNewick(args.tree)
    load_tree_db(db,tree, table=args.table_name)







