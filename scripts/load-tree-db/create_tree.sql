DROP DATABASE IF EXISTS tree_of_life;
CREATE DATABASE tree_of_life;
USE tree_of_life;
DROP TABLE IF EXISTS node;
CREATE TABLE node(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(511),
    tax_id INT(11),
    parent_id INT(11),
    extern_id VARCHAR(20),
    is_leaf BOOLEAN, 
    comment VARCHAR(255),
    length FLOAT,
    detail_level INT(4),
    depth INT(4),
    phylum_tax_id INT(11),
    class_tax_id INT(11),
    UNIQUE (name)
);

-- so that in group concat query, the ids are not overflown
SET group_concat_max_len = 8192;

CREATE TABLE manual_label(
    id INT(11) PRIMARY KEY,
    manual_rank VARCHAR(255),
    manual_tax_id INT(11),
    curator VARCHAR(255),
    manual_taxon_name VARCHAR(255)
);
