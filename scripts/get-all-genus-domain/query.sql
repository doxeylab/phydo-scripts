
-- first login by `ssh <USER>@doxtools.uwaterloo.ca`
-- mysql -u root -p
-- [INSIDE MYSQL INTERMINAL]
-- use tree_of_life

select t.genus_id as genus_id, tx.level as level, pr.pfamA_acc AS pfam_domain_acc  
from tree_of_life.pfam_tree t  
LEFT JOIN pfam.taxonomy tx ON t.genus_id = tx.ncbi_taxid 
LEFT JOIN pfam.pfamseq pq ON t.proteome_tax_id = pq.ncbi_taxid 
LEFT JOIN pfam.pfamA_reg_full_significant pr ON pq.pfamseq_acc = pr.pfamseq_acc 
WHERE t.proteome_tax_id is not null AND pr.pfamseq_acc is not null
INTO OUTFILE '/var/lib/mysql-files/<ANYNAME_OF_YOUR_CHOICE>' FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED by '\n';

select p.name, t.genus_id from pruned_node p left join pfam_tree t on p.id = t.tree_node_id where p.name is not null into outfile '/var/lib/mysql-files/name-to-genus-id.csv' fields terminated by ',' enclosed by '"' lines terminated by '\n';

-- note that files may contain duplicates, so use the following to remove duplicate.
-- bash awk '!x[$0]++' file > ~/my-file-without-duplicate-lines

