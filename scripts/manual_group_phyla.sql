-- manually tag some internal nodes as a specific phyla

-- 3010 to Firmicutes (1239)
update pruned_node SET phylum_tax_id = 1239 where id = 3010;

-- 12 to Proteobacteria (1224)
update pruned_node SET phylum_tax_id = 1224 where id = 12;

update pfam.taxonomy set parent = 200795 WHERE level = 'Thermobaculum';
UPDATE tree_of_life.pruned_node SET phylum_tax_id = 200795 WHERE id = 2828;