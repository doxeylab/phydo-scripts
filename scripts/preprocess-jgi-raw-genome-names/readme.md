given an html page that contain all jgi genome names classified by taxonomy, 
run the following 


```
# put jgi-all-taxonomy-id.html in the current directory
bash filter_taxon.sh 
python taxon_to_csv.py --jgi_filtered filtered --output_path taxons.csv
```
