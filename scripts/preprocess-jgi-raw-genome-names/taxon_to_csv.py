import re
import sys
import argparse
import csv 

parser = argparse.ArgumentParser(description="""
    Given a jgi taxonomy html page, scrapes the link containing relevant species info. 
    input: file with one <a> tag per line
    output: tab delimited file
    e.g. 
    name\ttaxoid\tlink
    Aigarchaeota archaeon JGI 0000001-A7 (contamination screened) (GBS-A_001_112)\t2264867219\thttps://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=TaxonDetail&amp;taxon_oid=2264867219
    """)
parser.add_argument('--jgi_filtered', dest='jgi_filtered',help='jgi html with only <a> tag', required=True)
parser.add_argument('--output_path', dest='output_path',help='output file',required=True)


def write_to_csv(path,rows,header):
    assert(len(rows) == 0 or len(rows[0]) == 3)
    with open(path,'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(header)
        for r in rows: 
            writer.writerow(r)


def get_taxoid(line):
    _species_link_re = 'https://img\.jgi\.doe\.gov/cgi-bin/m/main\.cgi\?section=TaxonDetail&amp;taxon_oid='
    result = re.search(_species_link_re + '(\d+)',line)
    return result.group(1)



def get_text(line):
    result = re.search('>(.*)</a>',line)
    return result.group(1)

def get_row(line):
    link = 'https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=TaxonDetail&amp;taxon_oid='
    taxoid = get_taxoid(line)
    name = get_text(line)
    return [name,taxoid,link+taxoid]

if __name__ == '__main__':
    args = parser.parse_args()
    with open(args.jgi_filtered,'r') as f:
        rows = [get_row(line) for line in f]
        write_to_csv(args.output_path,rows,['name','taxoid','link'])

