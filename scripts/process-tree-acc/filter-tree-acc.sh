if [ $# -ne 3 ];then
    echo "filters the rows with only IMG submission number"
    echo "usage $0 <accession number in tsv> <outputfile with IMG> <outputfile without IMG>"
    exit 1
fi


grep -P '^[^\t]+\t+\d{4,}' $1 > $2
grep -Pv '^[^\t]+\t+\d{4,}' $1 > $3

