#!/bin/bash

mysqldump --routines --triggers --add-drop-table --add-drop-database -u root -p --databases pfam tree_of_life > phydo-db-backup-$(date "+%Y-%m-%d").sql
