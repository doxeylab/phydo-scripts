#!/bin/bash

#Downloads all pfam database files and load them into datbase
# root user to mysql required
cd $HOME

if [ $# -ne 1 ]; then
    echo "Cannot find the password argument, aborting"
    exit 1
fi

pfamFTPDir="ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/"
pfamDbDir=$HOME/pfam_database_files

install_if_not_exist(){
	for file in $@; do
		# we must not have downloaded .gz file or have extracted those files
		if [ ! -r ${pfamDbDir}/${file} ] && [ ! -r ${pfamDbDir}/${file%.gz} ]; then
			echo "downloading ${file}"
			wget ${pfamFTPDir}${file} -O ${pfamDbDir}/${file}.temp
			mv ${pfamDbDir}/${file}.temp ${pfamDbDir}/${file}
		fi
	done
}

password=$1



if [ ! -d ${pfamDbDir} ]; then
    echo "Making pfam database file directory ${pfamDbDir}"
    mkdir ${pfamDbDir}
fi

echo "downloading required files"

install_if_not_exist {architecture,complete_proteomes,pfamseq,pfamA_reg_full_significant,taxonomy,pfamA}{.sql.gz,.txt.gz}

echo "Download complete"

cd ${pfamDbDir}
if [ -r "architecture.sql.gz" ]; then
    gunzip *.gz # will take a while
fi 

echo "Making a database in mysql called pfam" 
echo "DROP DATABASE IF EXISTS pfam; CREATE DATABASE pfam;" | mysql -u root -p${password}
cat *.sql | mysql -u root -p${password} -D pfam

echo "loading data"
mysqlimport --local -u root -p${password} pfam *.txt

echo "data loading complete, pfam db successfully created"

