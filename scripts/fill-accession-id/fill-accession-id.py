import argparse
import csv
parser = argparse.ArgumentParser(description="""
accepts two csv files, one 
is the complete list of tree leaf accession file,
the other is a list of tree leaves without accession""")
parser.add_argument('--tree_acc_file',required=True,dest='tree_acc_file')
parser.add_argument('--tree_without_acc_file',required=True,dest='tree_without_acc_file')
parser.add_argument('--output_file',required=True,dest='output_file')


def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1
    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]


def load_acc_to_mem(file_path):
    with open(file_path,'rb') as f:
        csvreader = csv.reader(f, delimiter='\t',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        lines = filter(lambda x: len(x) >= 2,[line for line in csvreader])
        return {line[0]:line[1] if len(line) > 1 else '' for line in lines}

def get_min_edit_distance_acc(all_acc,query_name):
    if query_name in all_acc:
        return all_acc[query_name]
    dists = [(levenshteinDistance(k,query_name),v) for k,v in all_acc.iteritems()]
    mdist = min(dists)
    edit_distance = mdist[0]
    if (edit_distance*1.0/len(query_name) > 0.4):
        return ''
    else:
        return mdist[1]

    

if __name__ == '__main__':
    args = parser.parse_args()
    accessions = load_acc_to_mem(args.tree_acc_file)
    with open(args.tree_without_acc_file,'rb') as f:
        csvreader = csv.reader(f,delimiter=',',quotechar='"')
        with open(args.output_file,'wb') as out:
            csvwriter = csv.writer(out,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
            for line in csvreader:
                print('computing for %s ' % line[1])
                acc = get_min_edit_distance_acc(accessions,line[1])
                print('got acc %s' % acc)
                csvwriter.writerow([line[0],line[1],acc])






