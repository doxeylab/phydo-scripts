import MySQLdb
from MySQLdb.constants import FIELD_TYPE

def getDb(username,password,host='localhost',database='tree_of_life'):
    _my_conv = { FIELD_TYPE.LONG: int,FIELD_TYPE.INT24:int}
    conn = MySQLdb.connect(user=username,passwd=password,host=host,db=database,conv=_my_conv)
    return conn



