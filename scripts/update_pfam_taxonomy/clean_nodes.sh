# extracts three columns of interest, <ncbi_taxid>,<parent_id>,<rank>

cat nodes.dmp | sed 's/\t//g' | awk -F'\|' '{print $1","$2","$3}' > nodes.cleaned.csv
