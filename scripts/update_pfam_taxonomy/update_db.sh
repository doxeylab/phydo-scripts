
if [ $# -ne 1 ]; then
	echo "Usage: $0 <mysql root password>"
	exit 1
fi

password=$1

taxonomyDir=$HOME/taxonomy_datafile
if [ ! -d $taxonomyDir ]; then
	mkdir $taxonomyDir
fi
if [ ! -r $taxonomyDir/names.dmp ] || [ ! -r $taxonomyDir/nodes.dmp ]; then
	wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdmp.zip -O $taxonomyDir/taxdmp.zip
	unzip $taxonomyDir/taxdmp.zip -d $HOME/taxonomy_datafile/
	cat $taxonomyDir/names.dmp | sed 's/\t//g' | awk -F'\|' '$4=="scientific name" {print $1","$2}' > $taxonomyDir/names.cleaned.csv
	cat $taxonomyDir/nodes.dmp | sed 's/\t//g' | awk -F'\|' '{print $1","$2","$3}' > $taxonomyDir/nodes.cleaned.csv
fi

NAMES_CLEANED=$taxonomyDir/names.cleaned.csv
NODES_CLEANED=$taxonomyDir/nodes.cleaned.csv
# a bit hard to read, but it works
# the temp table idea is taken from here: 
# http://stackoverflow.com/questions/15271202/mysql-load-data-infile-with-on-duplicate-key-update
echo "CREATE TEMPORARY TABLE temp_tax_node LIKE pfam.taxonomy;
LOAD DATA LOCAL INFILE '${NODES_CLEANED}' INTO TABLE temp_tax_node 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' 
LINES TERMINATED BY '\n' (ncbi_taxid, parent, rank);
CREATE TEMPORARY TABLE temp_tax_name LIKE pfam.taxonomy;
LOAD DATA LOCAL INFILE '${NAMES_CLEANED}' INTO TABLE temp_tax_name 
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' 
LINES TERMINATED BY '\n' (ncbi_taxid,level);

UPDATE temp_tax_node t1 JOIN temp_tax_name t2 ON t1.ncbi_taxid=t2.ncbi_taxid
SET t1.level = t2.level; 

INSERT INTO pfam.taxonomy (ncbi_taxid,parent,rank,level) SELECT 
t2.ncbi_taxid, t2.parent,t2.rank,t2.level FROM temp_tax_node t2 
WHERE NOT EXISTS (SELECT * FROM pfam.taxonomy WHERE ncbi_taxid = t2.ncbi_taxid);

" | mysql -u root -p$password -D pfam

#INSERT INTO pfam.taxonomy t1 (ncbi_taxid,parent,rank,species) SELECT 
# t2.ncbi_taxid, t2.parent,t2.rank,t2.species FROM temp_tax_node t2 WHERE NOT EXISTS 
# (SELECT * FROM pfam.taxonomy WHERE ncbi_taxid = t2.ncbi_taxid);

