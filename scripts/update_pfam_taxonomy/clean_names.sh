# extracts all taxonomy rows where the type is scientific name
# outputs csv without header <ncbi_taxid>,<name of organism>
cat names.dmp | sed 's/\t//g' | awk -F'\|' '$4=="scientific name" {print $1","$2}' > names.cleaned.csv



