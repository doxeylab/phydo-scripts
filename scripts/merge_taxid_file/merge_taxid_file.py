import argparse
import csv


parser = argparse.ArgumentParser(description="""
Accepts two csv files, two follow the same format
<name>,<external_id>,<taxonomy_id>
and both are WITHOUT header
outputs to `output`
rows in file1 will be replaced by rows in file2 if 
they have the same `name`
""")
parser.add_argument('--file1',required=True,dest='file1',help='file1')
parser.add_argument('--file2',required=True,dest='file2',help='file2')
parser.add_argument('--output',required=True,dest='output',help='output location')

def mergefile(file1, file2, output):
    with open(file1,'r') as f1:
        with open(file2, 'r') as f2:
            csvReader1 = csv.reader(f1,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
            csvReader2 = csv.reader(f2,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
            map1 = {line[0]:line for line in csvReader1}
            for l in csvReader2:
                map1[l[0]] = l
            with open(output,'wb') as o:
                csvWriter = csv.writer(o, delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
                for l in map1.values():
                    csvWriter.writerow(l)




if __name__ == '__main__':
    args = parser.parse_args()
    mergefile(args.file1,args.file2,args.output)
