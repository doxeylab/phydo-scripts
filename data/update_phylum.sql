UPDATE pfam.taxonomy SET rank = 'phylum' WHERE level in 
(
    'Stramenopiles',
    'Opisthokonta incertae sedis',
    'Apusozoa',
    'Alveolata',
    'Rhizaria',
    'Amoebozoa',
    'Breviatea',
    'Rhodophyta',
    'Katablepharidophyta',
    'Jakobida',
    'Heterolobosea',
    'Cryptophyta',
    'unclassified Archaea (miscellaneous)',
    'Candidatus Pacearchaeota',
    'candidate division WS6',
    'unclassified Bacteria (miscellaneous)',
    'candidate division SR1',
    'Parcubacteria',
    'Candidatus Melainabacteria',
    'Candidate division TA06'
);
