function-selective domain architecture plasticity potentials
=====================================
2014 Oct 30

Summary: associates GO term in SCOP, did a GO analysis across tree of life

Key Concepts
=======
- domain architecture plasticity: the number of unique domain architecture under a super family, defined as unique per superfamily
- genome level architecture diversity: the number of distinct architectures from the same superfamily found in one genome. This is per superfamily, per genome
    > how can different architectures (from the same superfamily) be present in a _genome_? Most proteins have a _single_ copy in one genome, it cannot have multiple architecture. It is common for Eukaryotes to have multiple architectures of protein from the same super family (say cytochrome, transcription factor)
- ancestral genomic architecture diversity: a matrix (row is superfamily, column is ancestral genome) where each element represents the _potential_ of superfamily to form different architectures in a given genome. (we cannot be sure of actual diversity, since ancestral genome is _estimated_ )
- plasticity potential for given domain set, per genome: 
    + Purpose: for a particular GO term, would the domains associated to that display higher plasticity potential? That is, would all domains, on average, be involved in many distinct architecture?
    + domain set is derived by retrieving all domains associated with a GO term
    + for each domain in the domain set, find how many different architectures it is involved in within that genome, calculate the median, PP_raw
        * median used to remove bias from extreme values
    + find all domains with a GO annotation in a genome, randomly sample n domains where n = size(domain_set), calculate PP_raw for each of sampled domain_set, do this B times 
    + calculate mean and standard deviation for B number of PP_raw's 
    + Calculate Z score of PP_raw using the mean and sd from random sampling (note that normal distribution is assumed), this is called **PP-score**
    + a P-value is also calculated by (number of times PP_raw > PP_raw_random_sampled) / (total number of sampling: B) [Note that this respects bootstraping method, and no normal assumption]
    + All the P-values corrected by multiple testing correction



Results
-----
- Most of time, domain set associated with a GO term display positive PP score, meaning that most of the time they have high plasticity (not sure if this is good, does it make sense for most GO terms to have positive score?? is it necessarily interesting? Evolution has accumulated many changes, not surprising the domain architecture is diverse.)
    + the fact that functionally related domain set display higher score than random domain set indicates what?

Important notes: 
----
When we say functional plasticity, do we mean this specific set of domains can be swapped easily to another context? 
But this is not very true, consider 
A B C 
A B D
A B E

A B will both get a high plasticity score because they are both present in many different architectures, but A or B alone are not very plastic, they require each other to function properly. 



>‘cis-acting’ mode experiences a steady increase along with increasing organism complexity

How should we interpret this statement? Do we say that cis-acting transcription regulators get swapped a lot between contexts? Proteins involving cis-acting functions obtained more diversity, relative to other domains? i.e. cis-acting proteins evolved more regulations and functions?

High PP:  evolution pressured proteins to evolve different functions/regulations with respect to that GO term
Average PP: evolution does not care if domains recombine to form diverse architectures, because there is no selection (other domains won't affect original function) 





Domain architecture evolution of pattern-recognition
receptors
====
2009, Immunogenetics

An Observational Study on Immune Protein Evolution

Pattern-recognition receptors are key components of immune system
There are two families of PRR:
* NOD-like (NLR)
* Toll-like (TLR)

* NLR and TLR experienced species-specific expansion, and domain shuffling
* Similar domain architecture is evolved independently: parallel evolution
* Only a few domains are functionally relevant, therefore the domain architecture choices are limited, resulting independent evolution of the same architecture


Questions: 

1. is it necessarily **limited number** of domains that cause parallel evolution? Could it be the case that selective pressure plays a role? 



Domain Tree-Based Analysis of Protein Architecture Evolution
=====
2008, Mol Biol Evol

Key question: How many times does reinvention of domain architecture occur?

Method: reconstruct ancestral protein from domain tree using a novel maximum parsimony method

Result: domain architecture reinvention is much more common, 12.4% of scored ? proteins with same architecture has multiple origins, 5.6% in domain-assigned protein. 

There is no strong functional bias (assume GO term analysis) found in convergently evolved protein (those with multiple origins)


Questions:

1. Why is convergently evolved protein interesting? What underlying dynamics does it reveal about evolution? 
1. Why is domain architecture re-invention common? Again, any explaining dynamics?


Evolutionary dynamics of protein domain
architecture in plants
=====
2012 BMC Evolutionary Biology

Studied 14 plant genomes on domain architecture
Results: similar distributions of species-specific, single-
domain, and multi-domain architectures. 65% of plant domain architectures present in ALL plant lineages. Remaining are lineage-specific. 

Data suggests that lineage-specific expansions are due to nuclear ploidy increase. Unique domain architectures reduce in number when genomes normalized to ancestral ones, without genome duplication. 


Question

1. whatever pattern you get from 14 genomes might not have generalizing power
1. Knowing that genome ploidy is one factor that causes domain expansion might not be interesting. How is it compared to other factors? What conclusions can we draw knowing that genome ploidy causes domain expansion? Isn't that same with any duplication event?


The Evolutionary History of Protein Domains Viewed by Species Phylogeny
===
2009 PlosOne

* Novel method for maximum parsimony domain history construction, based on gain, loss, vertical, and horizontal transfer. 
* Tested on 1015 genomes across tree of life. 
* Mapped to species tree to study domain architecture history, and its trend
* It is a **tool** that can be reused for horizontal gene transfer and more


Research Attention in Protein Domain Evolution
------
* distribution of single-domain and multidomain proteins in the three superkingdoms 
* domain duplication
* Convergent divergent evolution
* Domain evolution as recombination of existing architecture, and acquisition of new ones

consider domain evolution as: 

* changes to occurrence of domain within a genome
* changes to characteristics of domain
* Domain architecture change implies that domains are now under **different selective pressure** which in turn affects domain characteristics (it can be the other way around, variational domains might find advantage when combined with another domain, thus propagates) 
* the origin of novel protein can be inferred by origin of domains in the tree of life 

methodology 
-----
1. construct combination trees to reflect domain creation, recombination, duplication etc (species tree with domain label)
2. infer domain characteristic change, such as divergence of superfamily to sub family 
3. Infer domain composition in ancestors (assuming vertical inheritance, loss, horizontal gene transfer, and domain genesis). 
   - the result is that ancestor nodes, such as archaea, bacteria, have significantly more unique domains. It is concerning. 
   - it suggests that a lot of domains are invented in the root of tree? does not seem very plausible to me
4. investigate unique number of domains vs domain combination, question, how do you infer domain combination in ancestor nodes? it is weird

5. Assign a penalty score for vertical inheritance, loss, genesis/HGT, the parent node is one such that penalty score is minimized to get child domain compositions
6. domain combination is inferred using same procedure, (invention, loss, transfer etc, but I assume the penalty score is differnt) 



Examples of Studying
-----

#Divergence of superfamily: 

**Domain Superfamily Distribution vs Single Domain Distribtuion** 
Pilin and TcpA-like Pilin, Pilin is found across bacteria but not archaea, 
TcpA-like pilin is only found in _Vibrio cholera_ or _Vibrio fisheri_



Discussion (Limitations)
-------
* Their species tree is a taxonomy tree, so the exact distance between species are not well represented. There can be many many children under one node, and that node will have inflated unique domain count, as well as domain combination count 
    - say a domain is found in archaea and eukarya, their algorithm infers that the root of the tree contains this domain, and bacteria **lost** it. However, the domain might have evolved from the ancestor of eukarya and archaea. Bacteria never had it before
* Not every domain within a genome gets assigned. The average coverage is between 40 to 60%. (they used structural domain SCOP as opposed to sequence based domain pfam)
* when constructing a model to build phylogenetic trees, we don't have a ground truth and tuning hyper parameter such as domain genesis/HGT ratio can be hard
* Up to 20% to 30% of genome variation can be due to HGT. 


Evolution of Aminoacyl-tRNA Synthetases—Analysis of Unique Domain Architectures and Phylogenetic Trees Reveals a Complex History of Horizontal Gene Transfer Events
===
1999 Cold Spring Harbor Laboratory Press

20 aaRSs from across tree of life studied
Complex domain evolutionary history revealed partial domain homology from bacteria to archaea, 


Domain Combinations in Archaeal, Eubacterial and Eukaryotic Proteomes
============
2001 J. Mol. Biol
Abstract: 
    Studied SCOP domain in SCOP database. Most domains (783/859 superfamilies) are found in combination with another. A few are very very versatile. (Hypervariate). There are 209 families that do not combine with others. 
    Phylogenetic studies reveal more domains common to all three kingdoms are found, than lineage-specific ones. -> concludes recombination is driving evolution. [Note this is biased, because we might just have more understanding of the common domains, hence finding more of such domains]

Body:

* 20% domains in eukaryotes are single domained 
* Noticing that domain recombination network is a type of scale-free network, and an interesting property is that central hubs (ATP binding domain Rossman domains) have a **unique** repertoire of nodes connected to them. I.e. they might not share the same profile of nodes
* More recombinations occur in specific kingdoms




TreeFam v9: a new website, more species and
orthology-on-the-fly
====
2014 Oxford University Press

- They used D3
- They go over general technologies used for each feature

General Questions on Tools: 
- what problems to address? Why is another tool necessary?
- where to get data
- general tech used, and features they provide
- example results (picture)
- any benchmarks? memory, time, general performance
- how to use it? open source?
- Future directions

Inferring Horizontal Gene Transfer
=========
2015 CPLOSomputational Biology

Abstract Summary
---
Two computational methods:

* sequence composition-based, looks for codon usage/GC content, looks for deviation from genome average
* phylogenic approach: genes whose evolutionary history differs from host species. An example includes reconciliation between **species tree** and **gene tree** 
* Evaluation of algorithm relies on simulated genomes. Still very difficult to ascertain all but clear-cut HGT events

Body Summary
----
Explicit Phylogenic method compares gene tree and species tree
There are topological statistics that can be used: 

* Kishino-Hasegawa (KH)
* Shimodaira-Hasegawa (SH)
* Approximately Unbiased (AU) 
Asseses likelihood of _gene sequence alignment_ given reference topology as null hypothesis


Methodology article Detecting lateral gene transfers by statistical reconciliation of phylogenetic forests
=====
2015 
Prunier: a new method for phylogenetic detection of LGT based on the search for a maximum statistical agreement forest (MSAF) between a gene tree and a reference tree.



The Nature of Protein Domain Evolution: Shaping the Interaction Network
=====
2010 
A review for protein domain evolution with respect to comparative genomics and protein interaction networks (system biology)

Protein Phylogeny heavily dependent on Multiple Sequence alignment!
>A limitation of all inferred phylogenetic data is that it is directly dependent on the alignment and less so on the programs used to build the phylogenetic tree. See review: Multiple sequence alignment: in pursuit of homologous DNA positions.


On Interaction Network
----
* Types of interaction: 
    - Transcriptional, translational regulation,
    - intramolecular domain-domain interaction
    - post-translational protein modification
* 


The Natural Product Domain Seeker NaPDoS: A Phylogeny Based Bioinformatic Tool to Classify Secondary Metabolite Gene Diversity
=====
2012 PLOS one

Secondary Metabolite: not essential to an organism, but long-term deficiency impairs survivability, etc, or no change at all. 

A web tool Natural Product Domain Seeker (NaPDoS), which provides an automated method to assess the secondary metabolite biosynthetic gene diversity and novelty of strains or environments.

Use internal database to rapidly extract and characterize biosynthetic genes in genome/metagenome datasets. Results can help infer structures of metabolites and insights (based on phylogeny) on where to find new enzymes or synthesis mechanism. 


The Generation of New Protein Functions by the Combination of Domains
==========
Cell Press 2007, cited 100+ times

A comparison between single domain protein vs multidomain protein: 
1. similarity of function between single domain and multidomain
2. how functions combine to a new protein
3. results of combination:
    enzyme specificity, regulate activity

went thru 70 different proteins and compared functions for both multidomain and single domain

Mr Bayes
========
Likelihood function: 
* poisson continuous process 
* given branch length and mutation rates

prior: assume uniform distribution over tree space

Use MCMC (infact MC^3 heated MC) to infer posterio probability 





Kishino-Hasegawa Test (tree topology test)
-------
http://www.lacim.uqam.ca/~chauve/Enseignement/BIF7001/H05/PHYLOGENIE/BIF7001-Phylo-SystBiol49.pdf

Hypothesis: T1 and T2 are equally likely topologies

1. Find delta, which is the difference between likelihood L1 and L2
2. Resample data, for each bootstrap sample, calculate delta again
3. we have a distribution of delta_bootstrap now, now we compare the expectation of original delta (over the rest of free parameters) and the distribtion of bootstrap delta, we'd expect that E(delta_orig) = 0, and that should fall outside of 95% confidence interval, of delta_bootstrap, if T1 and T2 are very different topologies. **NOTE**, we have to do some bootstrap recentering here to make sure the test is correct. I don't really understand the detail

Time Saving Step: 
In the above test, because each bootstrap delta^i = L_1^i - L_2^i, and L_1^i is the maximum likelihood over any free parameter (e.g. branch length), such maximization over free parameter space is repeated for each bootstrap sampling and is inefficient. 
Instead, we can just use the free parameter set from the original data (not bootstrap) for each L_1_prime^i, and that is a good approximation for delta^i

We can further show that the original delta follows normal distribution N(0,v^2) where v is the variance, and can be inferred from the variance of recentered bootstrap estimator ~delta. 


HGT-Finder: A New Tool for Horizontal Gene Transfer Finding and Application to Aspergillus genomes
------

http://www.mdpi.com/2072-6651/7/10/4035/pdf

Basic Idea: 

Given query sequence Q, 
Blast to find set of top hits, each denoted by H_i
For each H_i calculate R_i and D_i
Where R_i = S'/S and S’ is the bit score hitting the best hit protein in Hi and S is the bit score for g hitting itself
D_i = N'/N   if Q has N levels in its taxonomic lineage (separated by “;” in example below) according to the NCBI taxonomy database, N’ will be the number of steps from the last level tracing back to the taxonomic level T containing both Q and Hi with respect to Q’s lineage e.g., N = 15, N’ = 7, Q = Aspergillus fumigatus, and Hi = Bipolaris maydis C5 in example below (T is leotiomyceta, denoted by “*”)


Q: cellular organisms; Eukaryota; Opisthokonta; Fungi; Dikarya; Ascomycota; saccharomyceta; Pezizomycotina; *leotiomyceta*; Eurotiomycetes; Eurotiomycetidae; Eurotiales; Aspergillaceae; Aspergillus; Aspergillus fumigatus
Hi: cellular organisms; Eukaryota; Opisthokonta; Fungi; Dikarya; Ascomycota; saccharomyceta; Pezizomycotina; *leotiomyceta*; dothideomyceta; Dothideomycetes; Pleosporomycetidae; Pleosporales; Pleosporineae; Pleosporaceae; Bipolaris; Bipolaris maydis;Bipolaris maydis C5.

The Horizontal Gene Transfer score is X = sum(R_i * D_i / i)/sqrt(K) (basically multiply R_i and D_i and divide that by index i, normalized by sqrt(K)), Note that the **top hit receives lowest i**.

DLIGHT
------

Model: there is a LGT event from donor species d to recipient r, happened at delta evolutionary distance ago. 
since that event, genes in d and r evolved at the the same rate. 

The set of significant LGT events are denoted by {(f,d,r)} where f is the gene family, d is donor, r is recipient, and the likelihood test statistics (under the null hypothesis that no LGT occured) for d,r must pass some threshold. (i.e. they significantly deviate from null hypothesis)

Basically this set says, give me all donor, recipient pair with significant enough LGT test statistics. 

The likelihood is calculated from l(f,d,r,delta), how likely is there a LGT event between d and r, happened at delta time ago. 

The null hypothesis is that when delta=infinity. 

This makes sense, because if gene in d, denoted by g_d is significantly different from g_r, AND, d and r are evolutionarily distant, then we are safe to infer a very large delta. 

But suppose that g_d and g_r are very similar, but the evolutionary distance between g and r are very far, then it is unlikely that delta would be large. 























































