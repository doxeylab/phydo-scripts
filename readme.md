About this repository
====
The repo contains many papers Han read and scripts used to load pfam and tree of life database.



In Scripts/
---

In `streamlined-db-setup.sh`. Where a `pfam` database and a `tree_of_life` database are set up provided that all dependencies are met. The up-to-update[as of Oct 11, 2017] input files can be found in `data/leaf-name-eid-tid.csv` and `data/unpruned_tree.newick`.

Note that some manual work has to be performed to load pruned tree into the database.

There are many scripts used to get ncbi genome ids from raw data from Laura Hug's tree of life paper. But that work is already done, in case you are interested, `clean-tree-accession`, `scrape_taxon_id` are the places to look at. 

> NOTE: the streamlined-db-setup does not contain all of the taxonomies used as of May 27, 2017. In `scripts/update_pfam_taxonomy`, we downloaded extra NCBI taxonomy dump and updated the `taxonomy` table that comes with default pfam database. 

In Data/
===

data/tree-acc-cleaned.tsv contains tree label name to NCBI taxonomy ID mapping
data/unpruned_tree.newick, data/pruned_tree.newick are tree of life newick files
